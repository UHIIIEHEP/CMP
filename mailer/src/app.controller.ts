import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern } from '@nestjs/microservices';
import { sendEmail } from './helper';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @EventPattern('send_hello_mail')
  async handleMessagePrinted(data: Record<string, unknown>) {
    await sendEmail(data.msg);
  }
}
