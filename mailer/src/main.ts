import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';

require('dotenv').config();

async function bootstrap() {

  const mailerName = process.env.RMQ_MAILER_NAME;
  const mailerUrl = process.env.RMQ_MAILER_URL;
  const mailerQueue = process.env.RMQ_MAILER_QUEUE;

  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: { 
      name: mailerName, 
      urls: [mailerUrl],
      queue: mailerQueue,
      queueOptions: { 
        durable: false
      },
    },
  });

  await app.listen(() => console.log('MoviesService MAILER is running.'));
}

bootstrap();
