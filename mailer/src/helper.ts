
const nodemailer = require('nodemailer');
require('dotenv').config();

const user = process.env.GUSER;
const pass = process.env.GPASS;


const transporter = nodemailer.createTransport({
  service: "Gmail",
  port: 465,
  secure: true,
  auth: {
    user,
    pass,
  }
});

const sendEmail = async (params: any) => {

  console.log(params)

  const message = `
    Привет <b>${params.user.firstname} ${params.user.patronymic}</b>! 
    Рады поприветсвовать тебя в <b>${params.user.department_name}</b> в качестве <b>${params.user.qualification_name}</b>.
    <br/>
    <hr>
    Для авторизации используй:<br/>
    логин: ${params.user.login} <br/>
    пароль: ${params.user.password} <br/>
  `

  const mailOptions = {
    from: 'no-replay@gmail.com',
    to: params.user.email,
    subject: 'Hello',
    html: message
  };

  console.log('SEND MESSAGE')
  await transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
    console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  })
}

export {
  sendEmail,
}