import { Transport, ClientsModule } from '@nestjs/microservices';

const mailerName = process.env.RMQ_MAILER_NAME;
const mailerUrl = process.env.RMQ_MAILER_URL;
const mailerQueue = process.env.RMQ_MAILER_QUEUE;

export const msProvider = ClientsModule.register([
  { 
    name: mailerName, 
    transport: Transport.RMQ,
    options: {
      urls: [mailerUrl],
      queue: mailerQueue,
      queueOptions: {
        durable: false,
        },
    },
  },
]);
