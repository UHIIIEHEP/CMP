import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { userProvider } from './user.provider';
import { DbModule } from '../../db/db.module';
import { msProvider } from '../../microservice.provider';
// import { Transport, ClientsModule } from '@nestjs/microservices';

@Module({
  imports: [
    DbModule,
    msProvider,
  ],
  controllers: [UserController],
  providers: [
    ...userProvider,
    UserService,
  ],
})
export class UserModule {}
