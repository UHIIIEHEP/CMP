import { Injectable } from '@nestjs/common';
import { PermissionCreateRequestDTO, PermissionGetRequestDTO, PermissionListRequestDTO, PermissionSelfGetRequestDTO, PermissionSetRequestDTO } from '../../dto/permission.dto';
import { Permission } from '../../entity/permission.entity';
import { dbQuery } from '../../helper';

@Injectable()
export class PermissionService {

  async create (
    payload: PermissionCreateRequestDTO,
  ): Promise<{permission: Permission}> {

    const {
      userInfoBySession: {user_id},
      alias,
      name,
    } = payload;

    const data = await permissionCreateCommon({
      user_id,
      alias,
      name,
    });
    
const permission = data;
    return { permission };
  }

  async list (
    payload: PermissionListRequestDTO,
  ): Promise<{permission: Permission[]}> {

    const {
      permission_id = null,
    } = payload;

    const permission = await permissionListCommon({
      permission_id,
    })

    return { permission };

  }

  async set(
    payload: PermissionSetRequestDTO,
  ): Promise<boolean> {

    const {
      userInfoBySession: {user_id},
      permission_id,
      department_id,
      qualification_id,
      append,
    } = payload;

    await permissionSetCommon({
      user_id,
      permission_id,
      department_id,
      qualification_id,
      append,
    })

    return true;
  }

  async get (
    payload: PermissionGetRequestDTO,
  ): Promise<{permission: Permission[]}> {
    const {
      user_id,
    } = payload;

    const permission = await permissionGetCommon({
      user_id
    });

    return { permission };
  }

  async selfGet (
    payload: PermissionSelfGetRequestDTO,
  ): Promise<{permission: Permission[]}> {
    const {
      userInfoBySession: {user_id},
    } = payload;

    const permission = await permissionGetCommon({
      user_id
    });

    return { permission };
  }
}

const permissionCreateCommon = async (params: {
  user_id: number;
  alias: string;
  name: string;
}): Promise<any> => {
  const [permission_id] = await dbQuery(
    'pkg_permission',
    'permission_create',
    [
      params.user_id,
      params.name,
      params.alias,
    ]
  )

  const [permission] = await permissionListCommon({
    permission_id: [permission_id.permission_create],
  })

  return permission;
}

const permissionListCommon = async (params: {
  permission_id?: number[];
}): Promise<any> => {
  return await dbQuery(
    'pkg_permission',
    'permission_list',
    [
      params.permission_id,
    ]
  )
}

const permissionSetCommon = async (params: {
  user_id: number,
  permission_id: number[];
  department_id: number;
  qualification_id: number;
  append?: Boolean;
}): Promise<any> => {

  const {
    user_id,
    permission_id,
    department_id,
    qualification_id,
    append,
  } = params;

  console.log({params})

   return await dbQuery(
     'pkg_permission',
     'permission_department_qualification_set',
      [
        user_id,
        permission_id,
        department_id,
        qualification_id,
        append,
      ]
   )
}

const permissionGetCommon = async (params: {
  user_id: number;
}) => {
  return await dbQuery(
    'pkg_permission',
    'user_permission_get',
    [
      params.user_id,
    ],
  );
}
