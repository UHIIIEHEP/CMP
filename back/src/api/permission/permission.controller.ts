import { Body, Controller, Post, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UserGuard } from '../../decorator/guard';
import {
  PermissionCreateRequestDTO,
  PermissionGetRequestDTO,
  PermissionListRequestDTO,
  PermissionSelfGetRequestDTO,
  PermissionSetRequestDTO,
} from '../../dto/permission.dto';
import { PermissionService } from './permission.service';

@Controller('permission')
export class PermissionController {
  constructor(private readonly permissionService: PermissionService) {}

  @Post('create')
  @ApiBearerAuth()
  @SetMetadata('permission', 'permission.write')
  @UseGuards(UserGuard)
  create(
    @Body() payload: PermissionCreateRequestDTO,
  ): any {
    return this.permissionService.create(payload);
  }

  @Post('list')
  @ApiBearerAuth()
  @SetMetadata('permission', 'permission.read')
  @UseGuards(UserGuard)
  list(
    @Body() payload: PermissionListRequestDTO,
  ): any {
    return this.permissionService.list(payload);
  }

  @Post('set')
  @ApiBearerAuth()
  @SetMetadata('permission', 'permission.write')
  @UseGuards(UserGuard)
  set(
    @Body() payload: PermissionSetRequestDTO,
  ): any {
    return this.permissionService.set(payload);
  }

  @Post('get')
  @ApiBearerAuth()
  @SetMetadata('permission', 'permission.read')
  @UseGuards(UserGuard)
  get(
    @Body() payload: PermissionGetRequestDTO,
  ): any {
    return this.permissionService.get(payload);
  }

  @Post('self/get')
  @ApiBearerAuth()
  @SetMetadata('permission', 'permission.read')
  @UseGuards(UserGuard)
  selfGet(
    @Body() payload: PermissionSelfGetRequestDTO,
  ): any {
    return this.permissionService.selfGet(payload);
  }

}
