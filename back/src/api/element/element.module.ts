import { Module } from '@nestjs/common';
// import { DbModule } from '../../db/db.module';
import { userProvider } from '../user/user.provider';
import { ElementController } from './element.controller';
import { elementProvider } from './element.provider';
import { ElementService } from './element.service';

@Module({
  imports: [
    // DbModule,
  ],
  controllers: [ElementController],
  providers: [
    ...userProvider,
    ...elementProvider,
    ElementService,
  ]
})
export class ElementModule {}
