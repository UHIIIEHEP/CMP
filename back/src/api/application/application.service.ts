import { Injectable } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { ApplicationCreateRequestDTO } from '../../dto/application.dto';
import { Application } from '../../entity/application.entity';

@Injectable()
export class ApplicationService {
  async create (
    payload: ApplicationCreateRequestDTO,
  ): Promise<{application: Application}> {
    const {
      userInfoBySession: {user_id},
      date_start,
      date_finish,
      name,
      description,
      parent_id,
      type,
      object_id,
    } = payload;

    const application = await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Application)
      .values([
        {
          created_by: user_id,
          created: new Date(),
          date_start,
          date_finish,
          name,
          description,
          parent_id,
          type,
          object_id,
        }
      ])
      .execute()
    
    return { application: application.raw[0], }
  }
}
