import { Body, Controller, Post, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthLoginRequestDTO } from '../../dto/auth.dto';
import { AuthService } from './auth.service';
import { Request } from 'express';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async authLogin(
    @Body() payload: AuthLoginRequestDTO,
    @Req() req: Request,
  ) : Promise<{token: string}> {
    return this.authService.login(payload, req);
  }

}
