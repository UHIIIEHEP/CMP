import { Body, Controller, Post, SetMetadata, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
} from '@nestjs/swagger';
import { UserGuard } from '../../decorator/guard';
import {
  CompetentRelationSetRequestDTO,
  CompetentRelationListRequestDTO,
  CompetentUserSelfListRequestDTO,
} from '../../dto/competent.dto';
import { Competent } from '../../entity/competent.entity';
import { CompetentService } from './competent.service';

@Controller('competent')
export class CompetentController {
  constructor(private readonly competentService: CompetentService) {}

  @Post('list')
  @ApiBearerAuth()
  @SetMetadata('permission', 'competent.read')
  @UseGuards(UserGuard)
  async competentList(
    @Body() payload,
  ): Promise<{competent: Competent[]}> {
    return this.competentService.list(payload);
  }

  @Post('relation/set')
  @ApiBearerAuth()
  @SetMetadata('permission', 'competent.relation.write')
  @UseGuards(UserGuard)
  async competentUserSet(
    @Body() payload: CompetentRelationSetRequestDTO,
  ): Promise<boolean> {

    console.log(1111)
    return this.competentService.competentRelationSet(payload);
  }

  @Post('relation/get')
  @ApiBearerAuth()
  @SetMetadata('permission', 'competent.relation.read')
  @UseGuards(UserGuard)
  async competentUserGet(
    @Body() payload: CompetentRelationListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    return this.competentService.competentRelationGet(payload);
  }

  @Post('user/self/get')
  @ApiBearerAuth()
  @SetMetadata('permission', 'competent.user.self.read')
  @UseGuards(UserGuard)
  async competentUserSelfGet(
    @Body() payload: CompetentUserSelfListRequestDTO,
  ): Promise<{competent: Competent[]}> {
    return this.competentService.competentUserSelfGet(payload);
  }
  
}
