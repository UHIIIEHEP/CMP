import { Module } from '@nestjs/common';
import { DbModule } from '../../db/db.module';
import { userProvider } from '../user/user.provider';
import { UserService } from '../user/user.service';
import { CompetentController } from './competent.controller';
import { competentProvider } from './competent.provider';
import { CompetentService } from './competent.service';

@Module({
  imports: [
    DbModule,
  ],
  controllers: [CompetentController],
  providers: [
    ...userProvider,
    ...competentProvider,
    UserService,
    CompetentService
  ],
})
export class CompetentModule {}
