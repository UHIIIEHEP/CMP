import { Module } from '@nestjs/common';
// import { DbModule } from '../../db/db.module';
import { userProvider } from '../user/user.provider';
import { ObjectController } from './object.controller';
import { objectProvider } from './object.provider';
import { ObjectService } from './object.service';

@Module({
  imports: [
    // DbModule,
  ],
  controllers: [ObjectController],
  providers: [
    ...userProvider,
    ...objectProvider,
    ObjectService,
  ]
})
export class ObjectModule {}
