import { Body, Controller, Post, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UserGuard } from '../../decorator/guard';
import { ObjectService } from './object.service';

@Controller('object')
export class ObjectController {
  constructor(private readonly objectService: ObjectService) {}

  @Post('create')
  @ApiBearerAuth()
  @SetMetadata('permission', 'object.write')
  @UseGuards(UserGuard)
  async objectCreate(
    @Body() payload,
  ): Promise<{object: object}> {
    return this.objectService.create(payload);
  }

  @Post('list')
  @ApiBearerAuth()
  @SetMetadata('permission', 'object.read')
  @UseGuards(UserGuard)
  async objectList(
    @Body() payload,
  ): Promise<{object: object[]}> {
    return this.objectService.list(payload);
  }
}
