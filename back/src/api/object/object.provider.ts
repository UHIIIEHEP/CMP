import { Connection } from 'typeorm';
import { Objects } from '../../entity/object.entity';

export const objectProvider = [
  {
    provide: 'OBJECT_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Objects),
    inject: ['DB_CONNECT'],
  },
]