import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('element')
export class Elements {
  @PrimaryGeneratedColumn()
  element_id: number;

  @Column({ nullable: true, type: 'integer' })
  parent_id?: number;

  @Column('character varying')
  name: string;

  @Column({ nullable: true, type: 'integer' })
  job_id: number;
}
