import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Permission } from "./permission.entity";
import { Qualification } from "./qualification.entity";
import { UserDepartmentQualificationSet } from "./user_department_qualification_set.entity";

@Entity('permission_department_qualification_set')
export class PermissionDepartmentQualificationSet {
  @PrimaryGeneratedColumn()
  permission_department_qualification_set_id: number;

  @Column('integer')
  department_id: number;

  @Column('integer')
  qualification_id: number;

  @Column('integer')
  permission_id: number;

  @ManyToOne(() => Permission, (permission) => permission.permission_id)
  @JoinColumn([{name: 'permission_id'}])
  permission: Permission;

  @ManyToOne(() => Qualification, (qualification) => qualification.qualification_id)
  @JoinColumn([{name: 'qualification_id'}])
  qualification: Qualification;

  @ManyToMany(
    () => UserDepartmentQualificationSet
  )
  @JoinColumn([
    { name: 'department_id' },
    { name: 'qualification_id' },
  ])
  udqs: UserDepartmentQualificationSet;
}
