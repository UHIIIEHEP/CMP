import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { PermissionDepartmentQualificationSet } from "./permission_department_qualif_set.entity";

@Entity('permission')
export class Permission {
  @PrimaryGeneratedColumn()
  permission_id: number;

  @Column('character varying')
  alias: string;

  @Column('character varying')
  name: string;

  // @ManyToOne(() => PermissionDepartmentQualificationSet)
  // @JoinColumn({name: 'permission_id'})
  // pdqs: PermissionDepartmentQualificationSet;
}
