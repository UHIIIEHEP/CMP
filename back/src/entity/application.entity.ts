import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('Application')
export class Application {
  @PrimaryGeneratedColumn()
  application_id: number;

  @Column({nullable: true, type: 'integer'})
  parent_id: number;

  @Column('character varying')
  type: string;

  @Column('character varying')
  name: string;

  @Column('text')
  description: string;

  @Column('timestamp with time zone')
  created: Date;

  @Column('integer')
  created_by: number;

  @Column('integer')
  object_id: number;

  @Column('timestamp with time zone')
  date_start: Date;

  @Column('timestamp with time zone')
  date_finish: Date;
}