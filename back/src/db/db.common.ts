import { NotFoundException } from '@nestjs/common';
import { DatabaseException } from '../exception/databaseException.exception';
import { DbSchema, DbSchemaParams } from './interfaces';

export class DbCommonService {
  protected db;

  private stripSlashes = (str: string): string => str.replace(/\\(.)/g, '$1');

  private parseParams = (
    schema: string,
    routine: string | number | symbol,
    params: {
      [key: string]: any;
    }[],
  ): { argString: string; argArray: any[] } => {
    const [procedureParams] = params;

    if (!procedureParams) {
      return {
        argString: '',
        argArray: [],
      };
    }

    let index = 1;
    const argArray = [];
    const argStringArray = [];

    for (const [key, value] of Object.entries(procedureParams)) {
      if (value === undefined) {
        const routineParams = DbSchemaParams[schema][routine];
        /*
        if argument is undefined and doesn't have a default value add null
        else add nothing
        */
        if (routineParams.includes(`${key}`)) {
          argArray.push(null);
          argStringArray.push(`${key} := $${index}`);
          index += 1;
        }
      } else {
        argArray.push(
          typeof value === 'string' ? this.stripSlashes(value) : value,
        );
        argStringArray.push(`${key} := $${index}`);
        index += 1;
      }
    }
    return {
      argString: argStringArray.join(', '),
      argArray,
    };
  };

  public async dbCall<
    K extends keyof DbSchema,
    P extends keyof DbSchema[K],
    R extends DbSchema[K][P] extends (...args: any[]) => any
      ? DbSchema[K][P]
      : never,
  >(
    schema: K,
    routine: P,
    ...params: Parameters<R> extends never ? [] : Parameters<R>
  ): Promise<ReturnType<R>> {
    try {
      const { argString, argArray } = this.parseParams(schema, routine, params);

      const query = `select * from ${schema}.${routine}(${argString});`;
      const queryResult = await this.db.query(query, argArray);

      if (!queryResult.length) {
        return [] as ReturnType<R>;
      }

      if (queryResult.length === 1) {
        const objectKeys = Object.keys(queryResult[0]);
        if (objectKeys.length === 1 && objectKeys[0] === routine) {
          return queryResult[0][routine];
        }
      }
      return queryResult;
    } catch (error) {
      if (/not_found/.test(error.message)) {
        throw new NotFoundException(error);
      }
      throw new DatabaseException(error);
    }
  }

  public dbObjectUnassociate = (data: any): Array<Array<any>> => {
    if (!data.length) {
      return [];
    }
    const names = Object.keys(data[0]);
    const values = data.map((row) => Object.values(row));
    return [names, values];
  };

  public dbObjectAssoc = (src) => {
    const fieldNames = src[0];
    return src[1].reduce((srcResult, srcValue) => {
      const item = {};

      srcValue.reduce((itemResult, itemValue, itemIndex) => {
        item[fieldNames[itemIndex]] = itemValue;
        return itemResult;
      }, {});

      srcResult.push(item);
      return srcResult;
    }, []);
  };

  public buildRoutineObjectArg = (json, prefix = 'p') => {
    if (json === null || typeof json === 'undefined') {
      return undefined;
    }

    return Object.entries(json).reduce((acc, el) => {
      const [key, value] = el;
      acc[`${prefix}_${key}`] = value;
      return acc;
    }, {});
  };
}
