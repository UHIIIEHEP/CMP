/*
AUTO GENERATED DO NOT MODIFY
"npm run create_interface" to update
*/

export enum pkg_analytics_analytics_template_type {
    DASHBOARD = 'dashboard',
    REPORT = 'report',
}

export enum pkg_history_action {
    CREATE = 'create',
    DELETE = 'delete',
    UPDATE = 'update',
}

export enum pkg_history_history_relation {
    OBJECT = 'object',
    PROJECT = 'project',
    SCHEDULE = 'schedule',
    SCHEDULE_FACT = 'schedule_fact',
}

export enum pkg_reference_job_element_group {
    CONSTRUCTION = 'construction',
    SITE = 'site',
    SPACE = 'space',
    SYSTEM = 'system',
}

export enum pkg_reference_job_unit {
    AR = 'ar',
    CENTIMETER = 'centimeter',
    CUBIC_CENTIMETER = 'cubic_centimeter',
    CUBIC_METER = 'cubic_meter',
    GRAM = 'gram',
    GRAM_PER_CUBIC_CENTIMETER = 'gram_per_cubic_centimeter',
    HECTARE = 'hectare',
    KILOGRAM = 'kilogram',
    KILOGRAM_PER_CUBIC_METER = 'kilogram_per_cubic_meter',
    KILOMETER = 'kilometer',
    KIT = 'kit',
    LINEAR_METER = 'linear_meter',
    LITER = 'liter',
    MEGAPASCAL = 'megapascal',
    METER = 'meter',
    MILLILITER = 'milliliter',
    MILLIMETER = 'millimeter',
    NONE = 'none',
    PASCAL = 'pascal',
    PERCENT = 'percent',
    PIECE = 'piece',
    SQUARE_CENTIMETER = 'square_centimeter',
    SQUARE_METER = 'square_meter',
    TON = 'ton',
}

export enum pkg_schedule_util_relation {
    OBJECT = 'object',
    SCHEDULE = 'schedule',
    SCHEDULE_FACT = 'schedule_fact',
    SCHEDULE_FACT_CORRECTION = 'schedule_fact_correction',
    SCHEDULE_PLAN = 'schedule_plan',
    SCHEDULE_VERSION = 'schedule_version',
}

export enum fact {
    FOREMAN = 'foreman',
    REVISOR = 'revisor',
}

export enum fact_correction_status {
    APPROVED = 'approved',
    ARCHIVED = 'archived',
    DRAFT = 'draft',
}

export enum fact_source {
    CORRECTION = 'correction',
    MOBILE = 'mobile',
    WEB = 'web',
}

export enum file_relation {
    OBJECT = 'object',
    OBJECT_FACT = 'object_fact',
    OBJECT_SCHEDULE = 'object_schedule',
    ORGANISATION = 'organisation',
    PROJECT = 'project',
    SCHEDULE = 'schedule',
    SCHEDULE_FACT = 'schedule_fact',
}

export enum guntt_calculation_method {
    ARITHMETIC_MEAN = 'arithmetic_mean',
    WEIGHTING_FACTOR = 'weighting_factor',
}

export enum image_ext {
    HEIC = 'heic',
    JFIF = 'jfif',
    JPEG = 'jpeg',
    JPG = 'jpg',
    PNG = 'png',
    WEBP = 'webp',
}

export enum image_relation {
    OBJECT = 'object',
    PROJECT = 'project',
    SCHEDULE_FACT = 'schedule_fact',
}

export enum relation {
    OBJECT = 'object',
    ORGANISATION = 'organisation',
    PROJECT = 'project',
    SCHEDULE_FACT = 'schedule_fact',
    USER = 'user',
}

export type pkg_report_schedule_guntt = {
    schedule_version_id: number,
    object_id: number,
    schedule_id: number,
    parent_id: number,
    parent_path: any,
    name: string,
    job: string,
    job_id: number,
    job_unit: string,
    date_start: Date,
    date_finish: Date,
    date_interval_day: number,
    volume_percent: number,
    volume_planned: number,
    volume_actual: number,
    volume_fact: number,
    weighting_factor: number,
    expenditure_planned: number,
    expenditure_actual: number,
    expenditure_fact: number,
    expenditure_percent: number,
    deviation_day: number,
    status: number,
    sort_order: number,
    num_order: number,    
};

export type dblink_pkey_results = {
    position: number,
    colname: string,    
};

export type DbSchema = {
  pkg_admin: {
    admin_auth(params: {
          p_email: string;
          p_password: string;
          p_address: string;
          p_client: string;
          p_salt?: string;
        }): number
    admin_check(params: {
          p_admin_id: number;
        }): number
    admin_data_get(params: {
          p_admin_id: number;
        }): {[key: string]: any}
    admin_data_set(params: {
          p_admin_id: number;
          p_data: {[key: string]: any};
        }): number
    admin_list(params: {
          p_admin_id: number[];
        }): {
            admin_id: number,
            firstname: string,
            lastname: string,
            email: string,
            phone: string,
          }[]
    admin_session_check(params: {
          p_session_id: number;
        }): number
    admin_session_create(params: {
          p_admin_id: number;
          p_address: string;
          p_client: string;
        }): number
    admin_session_delete(params: {
          p_admin_id: number;
          p_session_id: number[];
        }): number[]
    admin_session_list(params: {
          p_action_admin_id: number;
          p_admin_id: number[];
        }): {
            session_id: number,
            created: Date,
            admin_id: number,
            address: any,
            client: string,
          }[]
  }
  pkg_analytics: {
    analytics_template_check(params: {
          p_analytics_template_id: number;
          p_allow_null?: boolean;
        }): number
    analytics_template_create(params: {
          p_user_id: number;
          p_alias: string;
          p_permission: string;
          p_template_type: pkg_analytics_analytics_template_type;
          p_name: string;
          p_comment: string;
          p_template: {[key: string]: any};
          p_settings: {[key: string]: any};
          p_sort_order?: number;
        }): number
    analytics_template_delete(params: {
          p_user_id: number;
          p_analytics_template_id: number[];
        }): number[]
    analytics_template_list(params: {
          p_user_id: number;
          p_analytics_template_id: number[];
          p_filter?: {[key: string]: any};
        }): {
            analytics_template_id: number,
            alias: string,
            permission: string,
            template_type: pkg_analytics_analytics_template_type,
            name: string,
            comment: string,
            template: {[key: string]: any},
            settings: {[key: string]: any},
            sort_order: number,
          }[]
    analytics_template_update(params: {
          p_user_id: number;
          p_analytics_template_id: number;
          p_alias: string;
          p_permission: string;
          p_template_type: pkg_analytics_analytics_template_type;
          p_name: string;
          p_comment: string;
          p_template: {[key: string]: any};
          p_settings: {[key: string]: any};
          p_sort_order?: number;
        }): number
    analytics_template_user_template_list(params: {
          p_user_id: number;
          p_filter?: {[key: string]: any};
        }): {
            analytics_template_id: number,
            alias: string,
            permission: string,
            template_type: pkg_analytics_analytics_template_type,
            name: string,
            comment: string,
            template: {[key: string]: any},
            settings: {[key: string]: any},
            sort_order: number,
          }[]
    suek_daily_fact_entering_report(params: {
          p_user_id: number;
          p_date?: Date;
        }): {
            row_number: number,
            date: string,
            fact_count: number,
          }[]
    suek_daily_fact_entering_report_json(params: {
          p_user_id: number;
          p_date?: Date;
        }): {[key: string]: any}
    suek_fact_entering_report(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {
            schedule_fact_id: number,
            date_created: string,
            rpo: string,
            pe: string,
            project_name: string,
            object_id: number,
            object_name: string,
            schedule_name: string,
            job_name: string,
            job_unit: string,
            fact_volume: number,
            fact_expedenture: number,
            username: string,
            user_role: string,
            fact_source: string,
            image_count: number,
          }[]
    suek_fact_entering_report_json(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {[key: string]: any}
    suek_fact_entering_rpo_and_daily_report_json(params: {
          p_user_id: number;
          p_date?: Date;
        }): {[key: string]: any}
    suek_object_budget_daily_report(params: {
          p_user_id: number;
          p_date?: Date;
        }): {
            row_number: number,
            date: string,
            object_id: number,
            rpo_id: number,
            rpo: string,
            pe: string,
            object: string,
            workshift1: number,
            workshift2: number,
            year_plan: number,
            month_plan: number,
            oper_plan: number,
            day_plan: number,
            day_fact: number,
            day_plan_fact_diff: number,
            month_to_date_plan: number,
            month_to_date_fact: number,
            month_plan_fact_diff: number,
            month_plan_fact_diff_sd: number,
            year_to_date_plan: number,
            year_to_date_fact: number,
            year_plan_fact_diff: number,
            year_plan_fact_diff_sd: number,
          }[]
    suek_object_budget_daily_report_json(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {[key: string]: any}
    suek_object_detail_report(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {
            row_number: number,
            date: string,
            project_id: number,
            object_id: number,
            rpo: string,
            pe: string,
            project_name: string,
            object_name: string,
            volume_percent_plan_for_today: number,
            expenditure_plan_for_today: number,
            foreman_volume_percent: number,
            foreman_deviation_day: number,
            foreman_status: number,
            foreman_expenditure_fact: number,
            foreman_expenditure_percent: number,
            revisor_volume_percent: number,
            revisor_deviation_day: number,
            revisor_status: number,
            revisor_expenditure_fact: number,
            revisor_expenditure_percent: number,
            target: number,
            status: string,
            url: string,
          }[]
    suek_object_detail_report_json(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {[key: string]: any}
    suek_object_fact_status_report(params: {
          p_user_id: number;
          p_date?: Date;
        }): {
            row_number: number,
            date: string,
            object_id: number,
            serial_number: string,
            object: string,
            project: string,
            rpo: string,
            pe: string,
            queue: string,
            status: string,
            category: string,
            comment: string,
            foreman_fact: boolean,
            foreman_foto: boolean,
            revisor_fact: boolean,
            revisor_expenditure: boolean,
          }[]
    suek_object_fact_status_report_json(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {[key: string]: any}
    suek_object_readiness_report(params: {
          p_user_id: number;
          p_date_to: Date;
        }): {
            project_id: number,
            object_id: number,
            project_name_ru: string,
            object_name_ru: string,
            rpo: string,
            pe: string,
            queue: string,
            category: string,
            job_id: number,
            job_name_ru: string,
            is_job: string,
            date_start: string,
            date_finish: string,
            volume_planned: number,
            expenditure_planned: number,
            foreman_volume_fact: number,
            foreman_volume_percent: number,
            foreman_status: number,
            foreman_status_text: string,
            foreman_deviation_day: number,
            foreman_expenditure_fact: number,
            foreman_expenditure_percent: number,
            revisor_volume_fact: number,
            revisor_volume_percent: number,
            revisor_status: number,
            revisor_status_text: string,
            revisor_deviation_day: number,
            revisor_expenditure_fact: number,
            revisor_expenditure_percent: number,
            date_interval_day: number,
            gone_day: number,
            plan_volume_per_day: number,
            photo_1: string,
            photo_2: string,
            photo_3: string,
            photo_4: string,
            photo_5: string,
          }[]
    suek_object_readiness_report_json(params: {
          p_user_id: number;
          p_date_to?: Date;
        }): {[key: string]: any}
    suek_rpo_fact_entering_report(params: {
          p_user_id: number;
          p_date?: Date;
        }): {
            row_number: number,
            rpo: string,
            fact_count: number,
          }[]
    suek_rpo_fact_entering_report_json(params: {
          p_user_id: number;
          p_date?: Date;
        }): {[key: string]: any}
    suek_rpo_object_readiness_fact_image_list(params: {
          p_object_id: number;
        }): {
            image_id: number,
            object_id: number,
            schedule_id: number,
            created: string,
            job_name: string,
            image_url: string,
            user_name: string,
          }[]
    suek_rpo_object_readiness_fact_image_list_limit(params: {
          p_object_id: number;
          p_limit?: number;
        }): {
            image_id: number,
            object_id: number,
            schedule_id: number,
            created: string,
            job_name: string,
            image_url: string,
            user_name: string,
          }[]
    suek_rpo_object_readiness_report(params: {
          p_user_id: number;
          p_rpo_id: number;
          p_date_current: Date;
          p_date_previous: Date;
        }): {
            row_number: number,
            object_id: number,
            object_name: string,
            category: string,
            pe: string,
            deviation_day_diff: number,
            budget_ik: string,
            decision_ik: string,
            expenditure_planned: number,
            expenditure_fact: number,
            status_current: string,
            volume_percent_current: number,
            deviation_day_current: number,
            status_previous: string,
            volume_percent_previous: number,
            deviation_day_previous: number,
            stage_list: {[key: string]: any},
            job_list: {[key: string]: any},
            image_list: {[key: string]: any},
          }[]
    suek_rpo_object_readiness_report_json(params: {
          p_user_id: number;
          p_rpo_id: number;
          p_date_current: Date;
          p_date_previous: Date;
        }): {[key: string]: any}
  }
  pkg_auth: {
    generate_password_hash(params: {
          p_password: string;
          p_salt: string;
        }): any
    generate_password_sha256_hash(params: {
          p_password: string;
          p_salt: string;
        }): string
    session_check(params: {
          p_session_id: number;
        }): number
    session_create(params: {
          p_user_id: number;
          p_address: string;
          p_client: string;
        }): number
    session_delete(params: {
          p_user_id: number;
          p_session_id: number[];
        }): number[]
    session_list(params: {
          p_action_user_id: number;
          p_user_id: number[];
        }): {
            session_id: number,
            created: Date,
            user_id: number,
            address: any,
            client: string,
          }[]
  }
  pkg_db_util: {
    column_exists(params: {
          p_column: string;
          p_table: string;
          p_schema?: string;
        }): boolean
    grant_all_priveleges(params: {
          database: any;
          grant_to: any;
        }): void
    grant_read_priveleges(params: {
          database: any;
          grant_to: any;
        }): void
    pg_column_list(params: {
          p_schema?: string;
          p_table?: string;
        }): {
            schema: string,
            table: string,
            column: string,
            format: string,
            description: string,
          }[]
    pg_constraint_exists(params: {
          p_schema: string;
          p_table: string;
          p_constraint: string;
        }): boolean
    pg_constraint_list(params: {
          p_schema?: string;
          p_constraint_type?: string;
        }): {
            constraint_name: string,
            constraint_type: string,
            table_schema: string,
            table_name: string,
            column_name: string,
            foreign_table_schema: string,
            foreign_table_name: string,
            foreign_column_name: string,
          }[]
    pg_type_exists(params: {
          p_schema: string;
          p_typename: string;
        }): boolean
    restart_all_seq(): void
  }
  pkg_exception: {
    message_list(): string[]
    message_list_routine_create(): void
    parse_routine_definition(params: {
          p_body: string;
        }): string[]
    routine_definition(params: {
          p_schema: string;
          p_routine: string;
          p_arguments: string;
        }): string
    routine_exception_list(): string[]
    routine_list(params: {
          p_schema_pattern: string;
        }): {
            schema: string,
            name: string,
            arguments: string,
          }[]
  }
  pkg_experimental: {
    object_schedule_level_set(params: {
          p_object_id: number;
        }): void
    schedule_monthly_expenditure_list(params: {
          p_object_id: number;
        }): {
            schedule_id: number,
            name: string,
            date_start: string,
            date_finish: string,
            expenditure: number,
            month_count: number,
            month_expenditure: number,
            expenditure_by_month: {[key: string]: any},
          }[]
  }
  pkg_file: {
    file_check(params: {
          p_file_id: number;
          p_allow_null?: boolean;
        }): number
    file_create(params: {
          p_user_id: number;
          p_file_relation: file_relation;
          p_file_relation_id: number;
          p_parent_id?: number;
          p_public?: boolean;
        }): number
    file_delete(params: {
          p_user_id: number;
          p_file_id: number[];
        }): number[]
    file_list(params: {
          p_user_id: number;
          p_file_id: number[];
          p_deleted?: boolean;
        }): {
            file_id: number,
            file_relation: file_relation,
            file_relation_id: number,
            parent_id: number,
            folder: boolean,
            created: Date,
            created_by: number,
            mimetype: string,
            name: string,
            size: number,
            ext: string,
            digest: string,
            uploaded: boolean,
            meta: {[key: string]: any},
            public: boolean,
            deleted: number,
          }[]
    file_move(params: {
          p_user_id: number;
          p_file_id: number;
          p_parent_id: number;
          p_name?: string;
        }): void
    file_path(params: {
          p_file_id: number;
        }): {
            file_id: number,
            file_relation: file_relation,
            file_relation_id: number,
            parent_id: number,
            folder: boolean,
            created: Date,
            created_by: number,
            mimetype: string,
            name: string,
            size: number,
            ext: string,
            digest: string,
            uploaded: boolean,
            meta: {[key: string]: any},
          }[]
    file_relation_list(params: {
          p_user_id: number;
          p_file_relation: file_relation;
          p_file_relation_id: number[];
          p_parent_id: number;
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            file_id: number,
            file_relation: file_relation,
            file_relation_id: number,
            parent_id: number,
            folder: boolean,
            created: Date,
            created_by: number,
            mimetype: string,
            name: string,
            size: number,
            ext: string,
            digest: string,
            meta: {[key: string]: any},
            public: boolean,
            deleted: number,
          }[]
    file_update(params: {
          p_user_id: number;
          p_file_id: number;
          p_mimetype: string;
          p_name: string;
          p_size: number;
          p_ext: string;
          p_digest: string;
          p_uploaded: boolean;
          p_meta?: {[key: string]: any};
        }): number
    file_user_list(params: {
          p_user_id: number;
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            file_id: number,
            file_relation: file_relation,
            file_relation_id: number,
            parent_id: number,
            folder: boolean,
            created: Date,
            created_by: number,
            mimetype: string,
            name: string,
            size: number,
            ext: string,
            digest: string,
            meta: {[key: string]: any},
            deleted: number,
          }[]
    folder_create(params: {
          p_user_id: number;
          p_file_relation: file_relation;
          p_file_relation_id: number;
          p_name: string;
          p_parent_id?: number;
        }): number
    folder_delete(params: {
          p_user_id: number;
          p_file_id: number[];
        }): number[]
    folder_update(params: {
          p_user_id: number;
          p_file_id: number;
          p_name: string;
        }): number
  }
  pkg_history: {
    history_accessible_relations_list(params: {
          p_user_id: number;
        }): {
            project_id: number[],
            object_id: number[],
            schedule_id: number[],
            schedule_fact_id: number[],
          }[]
    history_action_enum_list(): string[]
    history_create(params: {
          p_user_id: number;
          p_action: pkg_history_action;
          p_history_relation: pkg_history_history_relation;
          p_history_relation_id: number;
          p_old_record: {[key: string]: any};
          p_new_record: {[key: string]: any};
        }): number
    history_filter_data(params: {
          p_user_id: number;
          p_history_relation: pkg_history_history_relation[];
        }): {
            name: string[],
            user: {[key: string]: any}[],
          }[]
    history_list(params: {
          p_user_id: number;
          p_filter: {[key: string]: any};
          p_sort: {[key: string]: any};
          p_limit: number;
          p_offset: number;
        }): {
            history_id: number,
            history_relation: pkg_history_history_relation,
            history_relation_id: number,
            created: Date,
            user: {[key: string]: any},
            action: pkg_history_action,
            old_record: {[key: string]: any},
            new_record: {[key: string]: any},
            total: number,
          }[]
    history_relation_enum_list(): string[]
    history_relation_list(params: {
          p_history_relation: pkg_history_history_relation;
          p_history_relation_id: number[];
        }): {
            history_id: number,
            history_relation: pkg_history_history_relation,
            history_relation_id: number,
            created: Date,
            user: {[key: string]: any},
            action: pkg_history_action,
            old_record: {[key: string]: any},
            new_record: {[key: string]: any},
          }[]
  }
  pkg_image: {
    image_check(params: {
          p_image_id: number;
          p_allow_null?: boolean;
        }): number
    image_convert_relation_list(params: {
          p_user_id: number;
          p_image_relation: image_relation;
        }): {
            image_convert_id: number,
            image_relation: image_relation,
            name: string,
            scenario: {[key: string]: any},
          }[]
    image_create(params: {
          p_user_id: number;
          p_image_relation: image_relation;
          p_image_relation_id: number;
        }): number
    image_delete(params: {
          p_user_id: number;
          p_image_id: number[];
        }): number[]
    image_list(params: {
          p_user_id: number;
          p_image_id: number[];
        }): {
            image_id: number,
            image_relation: image_relation,
            image_relation_id: number,
            created: Date,
            created_by: number,
            mimetype: string,
            name: string,
            size: number,
            ext: image_ext,
            digest: string,
            uploaded: boolean,
            converted: {[key: string]: any},
            main: boolean,
            meta: {[key: string]: any},
            geo_coord: {[key: string]: any},
          }[]
    image_relation_list(params: {
          p_user_id: number;
          p_image_relation: image_relation;
          p_image_relation_id: number[];
          p_sort: {[key: string]: any};
          p_main?: boolean;
        }): {
            image_id: number,
            image_relation: image_relation,
            image_relation_id: number,
            created: Date,
            created_by: number,
            mimetype: string,
            name: string,
            size: number,
            ext: image_ext,
            digest: string,
            converted: {[key: string]: any},
            main: boolean,
            geo_coord: {[key: string]: any},
          }[]
    image_set_main(params: {
          p_user_id: number;
          p_image_id: number;
        }): number
    image_update(params: {
          p_user_id: number;
          p_image_id: number;
          p_mimetype: string;
          p_name: string;
          p_size: number;
          p_ext: image_ext;
          p_digest: string;
          p_uploaded: boolean;
          p_converted?: {[key: string]: any};
          p_meta?: {[key: string]: any};
          p_geo_coord?: {[key: string]: any};
          p_exif?: {[key: string]: any};
        }): number
  }
  pkg_integration: {
    permission_list(params: {
          p_user_id: number;
          p_relation: string;
          p_relation_id?: number;
        }): {
            permission: string,
          }[]
    user_list(params: {
          p_page: number;
          p_per_page: number;
          p_user_id?: number[];
          p_search?: string;
          p_relation?: string;
          p_relation_id?: number;
        }): {
            user_id: number,
            name: string,
            email: string,
            logo: string,
            total: number,
          }[]
  }
  pkg_log: {
    exception_log_add(params: {
          p_code: string;
          p_message: string;
          p_query: string;
          p_fname?: string;
          p_label?: string;
        }): void
    exception_log_list(): {
            created: Date,
            code: string,
            message: string,
            fname: string,
            query: string,
          }[]
    exception_log_partition_create(params: {
          date_from: Date;
          count: number;
        }): void
  }
  pkg_object: {
    object_access_check(params: {
          p_user_id: number;
          p_object_id: number[];
        }): void
    object_check(params: {
          p_object_id: number;
          p_allow_null?: boolean;
        }): number
    object_create(params: {
          p_user_id: number;
          p_project_id: number;
          p_object_type_id: number;
          p_region_id: number;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_address: string;
          p_geo_coord: {[key: string]: any};
          p_broadcast_url: string;
          p_passport_data_ru: {[key: string]: any};
          p_passport_data_en: {[key: string]: any};
          p_calculation_method: guntt_calculation_method;
          p_meta: {[key: string]: any};
        }): number
    object_delete(params: {
          p_user_id: number;
          p_object_id: number[];
        }): number[]
    object_get_as_related_json(params: {
          p_object_id: number;
        }): {[key: string]: any}
    object_get_by_fact(params: {
          p_user_id: number;
          p_schedule_fact_id: number;
        }): number
    object_get_by_schedule(params: {
          p_user_id: number;
          p_schedule_id: number;
        }): number
    object_grant_access_to_user(params: {
          p_user_id: number;
          p_object_id: number;
          p_role_id: number[];
        }): void
    object_group_grant_access_to_user(params: {
          p_user_id: number;
          p_object_id: number[];
          p_role_id: number[];
        }): void
    object_list(params: {
          p_user_id: number;
          p_object_id: number[];
          p_project_id: number[];
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            object_id: number,
            project_id: number,
            object_type_id: number,
            region_id: number,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            address: string,
            geo_coord: {[key: string]: any},
            broadcast_url: string,
            date_start: Date,
            date_finish: Date,
            favorite: boolean,
            passport_data_ru: {[key: string]: any},
            passport_data_en: {[key: string]: any},
            calculation_method: guntt_calculation_method,
            weighting_factor: number,
            status_data: {[key: string]: any},
            settings: {[key: string]: any},
            meta: {[key: string]: any},
            deleted: number,
          }[]
    object_reset_weighting_factor(params: {
          p_user_id: number;
          p_object_id: number;
        }): number
    object_settings_set(params: {
          p_user_id: number;
          p_object_id: number;
          p_guntt_status_gray?: boolean;
        }): number
    object_transfer(params: {
          p_user_id: number;
          p_object_id: number;
          p_project_id: number;
        }): number
    object_update(params: {
          p_user_id: number;
          p_object_id: number;
          p_object_type_id: number;
          p_region_id: number;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_address: string;
          p_geo_coord: {[key: string]: any};
          p_broadcast_url: string;
          p_passport_data_ru: {[key: string]: any};
          p_passport_data_en: {[key: string]: any};
          p_calculation_method: guntt_calculation_method;
          p_meta: {[key: string]: any};
        }): number
    object_user_list(params: {
          p_user_id: number;
          p_project_id: number[];
          p_object_id?: number[];
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            object_id: number,
            project_id: number,
            object_type_id: number,
            region_id: number,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            address: string,
            geo_coord: {[key: string]: any},
            broadcast_url: string,
            date_start: Date,
            date_finish: Date,
            favorite: boolean,
            passport_data_ru: {[key: string]: any},
            passport_data_en: {[key: string]: any},
            calculation_method: guntt_calculation_method,
            weighting_factor: number,
            status_data: {[key: string]: any},
            settings: {[key: string]: any},
            meta: {[key: string]: any},
            deleted: number,
          }[]
    object_user_passport_filter_data(params: {
          p_user_id: number;
        }): {
            object_type_id: number,
            name: string,
            alias: string,
            fields: {[key: string]: any},
          }[]
  }
  pkg_organisation: {
    organisation_check(params: {
          p_organisation_id: number;
          p_allow_null?: boolean;
        }): number
    organisation_create(params: {
          p_user_id: number;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_meta: {[key: string]: any};
          p_settings?: {[key: string]: any};
          p_org_object_id?: string;
          p_external_data?: {[key: string]: any};
        }): number
    organisation_delete(params: {
          p_user_id: number;
          p_organisation_id: number[];
        }): number[]
    organisation_list(params: {
          p_user_id: number;
          p_organisation_id: number[];
          p_deleted?: boolean;
        }): {
            organisation_id: number,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            meta: {[key: string]: any},
            settings: {[key: string]: any},
            deleted: number,
          }[]
    organisation_update(params: {
          p_user_id: number;
          p_organisation_id: number;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_meta?: {[key: string]: any};
          p_settings?: {[key: string]: any};
        }): number
  }
  pkg_permission: {
    permission_alias_check(params: {
          p_permission_alias: string;
          p_allow_null?: boolean;
        }): number
    permission_check(params: {
          p_permission_id: number;
          p_allow_null?: boolean;
        }): number
    permission_create(params: {
          p_user_id: number;
          p_alias: string;
          p_name: string;
        }): number
    permission_delete(params: {
          p_user_id: number;
          p_permission_id: number[];
        }): number[]
    permission_list(params: {
          p_permission_id: number[];
        }): {
            permission_id: number,
            alias: string,
            name: string,
            deleted: number,
          }[]
    permission_update(params: {
          p_user_id: number;
          p_permission_id: number;
          p_alias: string;
          p_name: string;
        }): number
    role_access_check(params: {
          p_user_id: number;
          p_role_id: number[];
        }): void
    role_alias_check(params: {
          p_role_alias: string;
          p_allow_null?: boolean;
        }): number
    role_check(params: {
          p_role_id: number;
          p_allow_null?: boolean;
        }): number
    role_create(params: {
          p_user_id: number;
          p_alias: string;
          p_name: string;
          p_description?: string;
          p_organisation_id?: number;
        }): number
    role_create_default_role_with_permission(): void
    role_create_with_permission(params: {
          p_role_alias: string;
          p_role_name: string;
          p_permission_list: string[];
        }): void
    role_delete(params: {
          p_user_id: number;
          p_role_id: number[];
        }): number[]
    role_list(params: {
          p_user_id: number;
          p_role_id: number[];
          p_organisation_id?: number;
        }): {
            role_id: number,
            alias: string,
            name: string,
            description: string,
            organisation_id: number,
          }[]
    role_permission_list(params: {
          p_user_id: number;
          p_role_id: number[];
          p_organisation_id?: number;
          p_deleted?: boolean;
        }): {
            role_id: number,
            permission_id: number,
            permission_alias: string,
            permission_name: string,
            deleted: number,
          }[]
    role_permission_set(params: {
          p_user_id: number;
          p_role_id: number;
          p_permission_id: number[];
          p_append?: boolean;
        }): number
    role_set_to_default_accounts(): void
    role_total_permission_reset(): void
    role_update(params: {
          p_user_id: number;
          p_role_id: number;
          p_alias: string;
          p_name: string;
          p_description: string;
          p_organisation_id: number;
        }): number
  }
  pkg_project: {
    project_access_check(params: {
          p_user_id: number;
          p_project_id: number[];
        }): void
    project_check(params: {
          p_project_id: number;
          p_allow_null?: boolean;
        }): number
    project_create(params: {
          p_user_id: number;
          p_organisation_id: number;
          p_name_ru: string;
          p_name_en: string;
          p_region_id: number;
          p_project_type_id: number;
          p_description_ru: string;
          p_description_en: string;
          p_passport_data_ru: {[key: string]: any};
          p_passport_data_en: {[key: string]: any};
          p_meta: {[key: string]: any};
          p_project_guid?: string;
          p_external_data?: {[key: string]: any};
        }): number
    project_delete(params: {
          p_user_id: number;
          p_project_id: number[];
        }): number[]
    project_get_as_related_json(params: {
          p_project_id: number;
        }): {[key: string]: any}
    project_grant_access_to_user(params: {
          p_user_id: number;
          p_project_id: number;
          p_role_id: number[];
        }): void
    project_group_grant_access_to_user(params: {
          p_user_id: number;
          p_project_id: number[];
          p_role_id: number[];
          p_grant_object: boolean;
        }): void
    project_list(params: {
          p_user_id: number;
          p_project_id: number[];
          p_organisation_id: number[];
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            project_id: number,
            organisation_id: number,
            name_ru: string,
            name_en: string,
            project_type_id: number,
            region_id: number,
            description_ru: string,
            description_en: string,
            date_start: Date,
            date_finish: Date,
            favorite: boolean,
            passport_data_ru: {[key: string]: any},
            passport_data_en: {[key: string]: any},
            settings: {[key: string]: any},
            status_data: {[key: string]: any},
            meta: {[key: string]: any},
            deleted: number,
          }[]
    project_permission_list(params: {
          p_user_id: number;
          p_relation: relation;
          p_relation_id?: number[];
        }): {
            relation_id: number,
            permission: string[],
          }[]
    project_settings_set(params: {
          p_user_id: number;
          p_project_id: number;
          p_round_value?: boolean;
          p_round_percent?: boolean;
          p_round_expenditure?: boolean;
          p_round_day?: boolean;
          p_schedule_update_requires_comment?: boolean;
        }): number
    project_transfer(params: {
          p_user_id: number;
          p_project_id: number;
          p_organisation_id: number;
        }): number
    project_update(params: {
          p_user_id: number;
          p_project_id: number;
          p_name_ru: string;
          p_name_en: string;
          p_region_id: number;
          p_project_type_id: number;
          p_description_ru: string;
          p_description_en: string;
          p_passport_data_ru: {[key: string]: any};
          p_passport_data_en: {[key: string]: any};
          p_meta: {[key: string]: any};
        }): number
    project_user_list(params: {
          p_user_id: number;
          p_project_id?: number[];
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            project_id: number,
            organisation_id: number,
            name_ru: string,
            name_en: string,
            project_type_id: number,
            region_id: number,
            description_ru: string,
            description_en: string,
            date_start: Date,
            date_finish: Date,
            favorite: boolean,
            passport_data_ru: {[key: string]: any},
            passport_data_en: {[key: string]: any},
            settings: {[key: string]: any},
            status_data: {[key: string]: any},
            meta: {[key: string]: any},
            deleted: number,
          }[]
    project_user_passport_filter_data(params: {
          p_user_id: number;
        }): {
            project_type_id: number,
            name: string,
            alias: string,
            fields: {[key: string]: any},
          }[]
  }
  pkg_reference: {
    calculation_method_list(): string[]
    element_check(params: {
          p_element_id: number;
          p_allow_null?: boolean;
        }): number
    element_create(params: {
          p_user_id: number;
          p_code: string;
          p_key: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_job_element_group: string;
        }): number
    element_delete(params: {
          p_user_id: number;
          p_element_id: number[];
        }): number[]
    element_list(params: {
          p_user_id: number;
          p_element_id: number[];
          p_filter?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            element_id: number,
            key: string,
            code: string,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            job_element_group: pkg_reference_job_element_group,
            deleted: number,
          }[]
    element_update(params: {
          p_user_id: number;
          p_element_id: number;
          p_code: string;
          p_key: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_job_element_group: string;
        }): number
    fact_correction_status_list(): string[]
    fact_type_list(): string[]
    image_relation_type_list(): string[]
    job_check(params: {
          p_job_id: number;
          p_allow_null?: boolean;
        }): number
    job_create(params: {
          p_user_id: number;
          p_code: string;
          p_key: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_job_element_group: string;
          p_unit: string;
        }): number
    job_delete(params: {
          p_user_id: number;
          p_job_id: number[];
        }): number[]
    job_element_group_list(): string[]
    job_list(params: {
          p_user_id: number;
          p_job_id: number[];
          p_filter?: {[key: string]: any};
          p_deleted?: boolean;
        }): {
            job_id: number,
            key: string,
            code: string,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            job_element_group: pkg_reference_job_element_group,
            job_unit: pkg_reference_job_unit,
            deleted: number,
          }[]
    job_unit_list(): string[]
    job_update(params: {
          p_user_id: number;
          p_job_id: number;
          p_code: string;
          p_key: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_job_element_group: pkg_reference_job_element_group;
          p_unit: pkg_reference_job_unit;
        }): number
    object_type_check(params: {
          p_object_type_id: number;
          p_allow_null?: boolean;
        }): number
    object_type_create(params: {
          p_user_id: number;
          p_alias: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru?: string;
          p_description_en?: string;
          p_passport_template_ru?: {[key: string]: any};
          p_passport_template_en?: {[key: string]: any};
        }): number
    object_type_delete(params: {
          p_user_id: number;
          p_object_type_id: number[];
        }): number[]
    object_type_list(params: {
          p_object_type_id: number[];
          p_deleted?: boolean;
        }): {
            object_type_id: number,
            alias: string,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            passport_template_ru: {[key: string]: any},
            passport_template_en: {[key: string]: any},
            deleted: number,
          }[]
    object_type_update(params: {
          p_user_id: number;
          p_object_type_id: number;
          p_alias: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_passport_template_ru: {[key: string]: any};
          p_passport_template_en: {[key: string]: any};
        }): number
    project_type_check(params: {
          p_project_type_id: number;
          p_allow_null?: boolean;
        }): number
    project_type_create(params: {
          p_user_id: number;
          p_alias: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_passport_template_ru?: {[key: string]: any};
          p_passport_template_en?: {[key: string]: any};
        }): number
    project_type_delete(params: {
          p_user_id: number;
          p_project_type_id: number[];
        }): number[]
    project_type_list(params: {
          p_project_type_id: number[];
          p_deleted?: boolean;
        }): {
            project_type_id: number,
            alias: string,
            name_ru: string,
            name_en: string,
            description_ru: string,
            description_en: string,
            passport_template_ru: {[key: string]: any},
            passport_template_en: {[key: string]: any},
            deleted: number,
          }[]
    project_type_update(params: {
          p_user_id: number;
          p_project_type_id: number;
          p_alias: string;
          p_name_ru: string;
          p_name_en: string;
          p_description_ru: string;
          p_description_en: string;
          p_passport_template_ru: {[key: string]: any};
          p_passport_template_en: {[key: string]: any};
        }): number
    region_list(params: {
          p_region_id: number[];
          p_deleted?: boolean;
        }): {
            region_id: number,
            name: string,
            deleted: number,
          }[]
  }
  pkg_report: {
    report_fact_foto(params: {
          p_user_id: number;
          p_object_id: number;
          p_date_from?: Date;
          p_date_to?: Date;
          p_fact_type?: fact;
        }): {
            image_id: number,
            schedule_fact_id: number,
            fact_type: fact,
            fact_source: fact_source,
            created: Date,
            image: {[key: string]: any},
            job_id: number,
            job_unit: pkg_reference_job_unit,
            job_name: string,
            volume: number,
            expenditure: number,
            comment: string,
            lastname_ru: string,
            firstname_ru: string,
            patronymic_ru: string,
            lastname_en: string,
            firstname_en: string,
            patronymic_en: string,
          }[]
    report_fact_frequency(params: {
          p_user_id: number;
          p_object_id: number;
          p_date_from: Date;
          p_date_to: Date;
          p_fact_type: fact;
          p_allow_expenditure?: boolean;
          p_fact_source?: fact_source;
        }): {
            schedule_id: number,
            name: string,
            volume_planned: number,
            expenditure_planned: number,
            job_unit: pkg_reference_job_unit,
            volume_fact: number,
            expenditure_fact: number,
            fact_count: {[key: string]: any},
          }[]
    report_object_arrow(params: {
          p_user_id: number;
          p_object_id: number;
          p_schedule_version_id?: number;
          p_fact_type?: fact;
          p_calculation_method?: guntt_calculation_method;
        }): {[key: string]: any}
    report_object_arrow_list(params: {
          p_user_id: number;
          p_report_arrow_id: number;
          p_schedule_version_id?: number;
          p_fact_type?: fact;
          p_calculation_method?: guntt_calculation_method;
        }): {
            title: string,
            topEndDate: Date,
            bottomEndDate: Date,
            bottomColor: string,
          }[]
    report_object_guntt(params: {
          p_user_id: number;
          p_object_id: number;
          p_schedule_version_id?: number;
          p_fact_type?: fact;
          p_calculation_method?: guntt_calculation_method;
          p_allow_expenditure?: boolean;
          p_date_to?: Date;
        }): pkg_report_schedule_guntt[]
    report_object_guntt_fact_sum(params: {
          p_schedule_id: number[];
          p_fact_type: fact;
          p_date_to: Date;
        }): {
            schedule_id: number,
            volume_fact: number,
            expenditure_fact: number,
          }[]
    report_object_guntt_list(params: {
          p_object_id: number;
          p_schedule_version_id?: number;
          p_allow_expenditure?: boolean;
        }): pkg_report_schedule_guntt[]
  }
  pkg_schedule: {
    schedule_check(params: {
          p_schedule_id: number;
          p_allow_null?: boolean;
        }): number
    schedule_create(params: {
          p_user_id: number;
          p_object_id: number;
          p_schedule_version_id: number;
          p_parent_id: number;
          p_name: string;
          p_job_id: number;
          p_job_unit: pkg_reference_job_unit;
          p_element_id: number;
          p_date_start: Date;
          p_date_finish: Date;
          p_volume_actual: number;
          p_expenditure_actual: number;
          p_volume_planned: number;
          p_expenditure_planned: number;
          p_weighting_factor?: number;
          p_reset_weighting_factor?: boolean;
          p_meta?: {[key: string]: any};
          p_comment?: string;
        }): number
    schedule_delete(params: {
          p_user_id: number;
          p_schedule_id: number[];
          p_reset_weighting_factor?: boolean;
          p_comment?: string;
        }): number[]
    schedule_get_as_related_json(params: {
          p_schedule_id: number;
          p_schedule_version_id?: number;
        }): {[key: string]: any}
    schedule_list(params: {
          p_user_id: number;
          p_object_id: number;
          p_schedule_id: number[];
          p_schedule_version_id: number;
          p_allow_expenditure?: boolean;
          p_allow_weighting_factor?: boolean;
          p_deleted?: boolean;
          p_filter?: {[key: string]: any};
        }): {
            schedule_version_id: number,
            schedule_id: number,
            parent_id: number,
            object_id: number,
            name: string,
            job_id: number,
            job_unit: string,
            element_id: number,
            date_start: string,
            date_finish: string,
            volume_actual: number,
            volume_planned: number,
            expenditure_actual: number,
            expenditure_planned: number,
            weighting_factor: number,
            meta: {[key: string]: any},
            sort_order: number,
            parent_path: string,
            created_by: number,
            deleted: number,
          }[]
    schedule_load(params: {
          p_user_id: number;
          p_object_id: number;
          p_filename: string;
          p_data: {[key: string]: any};
          p_expenditure_multiplier?: number;
        }): number
    schedule_move(params: {
          p_user_id: number;
          p_schedule_id: number;
          p_parent_id: number;
          p_after_schedule_id: number;
        }): void
    schedule_organisation_user_check(params: {
          p_schedule_id: number;
          p_user_id: number;
        }): void
    schedule_path_get(params: {
          p_object_id: number;
          p_parent_id: number;
        }): any
    schedule_path_piece_length(): number
    schedule_path_reset_by_sort_order(params: {
          p_object_id: number;
          p_parent_id: number;
          p_path: any;
        }): void
    schedule_sort_step(): number
    schedule_update(params: {
          p_user_id: number;
          p_schedule_id: number;
          p_schedule_version_id: number;
          p_name: string;
          p_job_id: number;
          p_job_unit: pkg_reference_job_unit;
          p_element_id: number;
          p_date_start: Date;
          p_date_finish: Date;
          p_volume_actual: number;
          p_expenditure_actual: number;
          p_volume_planned: number;
          p_expenditure_planned: number;
          p_weighting_factor?: number;
          p_reset_weighting_factor?: boolean;
          p_meta?: {[key: string]: any};
          p_comment?: string;
        }): number
  }
  pkg_schedule_fact: {
    fact_access_check(params: {
          p_user_id: number;
          p_schedule_fact_id: number[];
        }): void
    fact_check(params: {
          p_schedule_fact_id: number;
          p_allow_null?: boolean;
        }): number
    fact_create(params: {
          p_user_id: number;
          p_schedule_id: number;
          p_fact_type: fact;
          p_fact_source: fact_source;
          p_volume: number;
          p_expenditure: number;
          p_employee: number;
          p_comment?: string;
          p_created?: Date;
        }): number
    fact_delete(params: {
          p_user_id: number;
          p_schedule_fact_id: number[];
        }): number[]
    fact_get_as_related_json(params: {
          p_schedule_fact_id: number;
        }): {[key: string]: any}
    fact_list(params: {
          p_user_id: number;
          p_schedule_fact_id: number[];
          p_filter?: {[key: string]: any};
          p_sort?: {[key: string]: any};
          p_allow_expenditure?: boolean;
          p_allow_employee?: boolean;
          p_deleted?: boolean;
        }): {
            schedule_fact_id: number,
            created: Date,
            user_id: number,
            schedule_id: number,
            fact_type: fact,
            fact_source: fact_source,
            volume: number,
            expenditure: number,
            employee: number,
            comment: string,
            deleted: number,
          }[]
    fact_update(params: {
          p_user_id: number;
          p_schedule_fact_id: number;
          p_volume: number;
          p_expenditure: number;
          p_employee: number;
          p_comment?: string;
        }): number
  }
  pkg_schedule_fact_correction: {
    fact_correction_check(params: {
          p_schedule_fact_correction_id: number;
          p_allow_null?: boolean;
        }): number
    fact_correction_create(params: {
          p_user_id: number;
          p_object_id: number;
          p_status: fact_correction_status;
          p_date_from: Date;
          p_date_to: Date;
        }): number
    fact_correction_data_check(params: {
          p_schedule_fact_correction_data_id: number;
          p_allow_null?: boolean;
        }): number
    fact_correction_data_get(params: {
          p_user_id: number;
          p_schedule_fact_correction_id: number;
        }): {
            schedule_fact_correction_data_id: number,
            schedule_fact_correction_id: number,
            schedule_id: number,
            name: string,
            job_unit: pkg_reference_job_unit,
            volume: number,
            expenditure: number,
          }[]
    fact_correction_data_load(params: {
          p_user_id: number;
          p_object_id: number;
          p_date_from: Date;
          p_date_to: Date;
        }): {
            schedule_id: number,
            name: string,
            job_unit: pkg_reference_job_unit,
            volume: number,
            expenditure: number,
          }[]
    fact_correction_data_set(params: {
          p_user_id: number;
          p_schedule_fact_correction_id: number;
          p_data: {[key: string]: any}[];
        }): number
    fact_correction_list(params: {
          p_user_id: number;
          p_object_id: number;
          p_schedule_fact_correction_id: number[];
          p_filter: {[key: string]: any};
          p_sort: {[key: string]: any};
          p_limit: number;
          p_offset: number;
        }): {
            schedule_fact_correction_id: number,
            created: Date,
            created_by: number,
            user: {[key: string]: any},
            object_id: number,
            status: fact_correction_status,
            date_from: string,
            date_to: string,
            total: number,
          }[]
    fact_correction_update(params: {
          p_user_id: number;
          p_schedule_fact_correction_id: number;
          p_status: fact_correction_status;
          p_date_from: Date;
          p_date_to: Date;
        }): number
  }
  pkg_schedule_plan: {
    plan_check(params: {
          p_schedule_plan_id: number;
          p_allow_null?: boolean;
        }): number
    plan_checkup(params: {
          p_user_id: number;
          p_schedule_id: number;
          p_schedule_version_id: number;
          p_date_start: Date;
          p_date_finish: Date;
          p_volume_planned: number;
          p_expenditure_planned: number;
        }): number
    plan_create(params: {
          p_user_id: number;
          p_schedule_id: number;
          p_schedule_version_id: number;
          p_date_start: Date;
          p_date_finish: Date;
          p_volume_planned: number;
          p_expenditure_planned: number;
        }): number
    plan_delete(params: {
          p_user_id: number;
          p_schedule_plan_id: number;
        }): number
    plan_list(params: {
          p_user_id: number;
          p_schedule_plan_id: number[];
          p_schedule_id: number[];
        }): {
            schedule_plan_id: number,
            schedule_id: number,
            name: string,
            meta: {[key: string]: any},
          }[]
  }
  pkg_schedule_util: {
    job_deviation_day(params: {
          p_date_start: Date;
          p_date_finish: Date;
          p_volume_fact_and_actual: number;
          p_volume_planned: number;
        }): number
    job_status(params: {
          p_job_id: number;
          p_date_start: Date;
          p_date_finish: Date;
          p_volume_fact: number;
          p_volume_planned: number;
          p_date_to?: Date;
        }): number
    job_volume_percent(params: {
          p_volume_planned: number;
          p_volume_actual: number;
          p_calculation_method: guntt_calculation_method;
          p_weighting_factor: number;
        }): number
    object_calculate_and_update_guntt_fields(params: {
          p_object_id?: number[];
          p_project_id?: number[];
        }): number
    project_calculate_and_update_guntt_fields(params: {
          p_project_id?: number[];
        }): number
    relation_calculate_and_update_guntt_fields(params: {
          p_relation: pkg_schedule_util_relation;
          p_relation_id: number;
        }): void
    schedule_fact_frequency(params: {
          p_schedule_id: number;
          p_date_from: Date;
          p_date_to: Date;
          p_fact_type: fact;
          p_allow_expenditure?: boolean;
          p_fact_source?: fact_source;
        }): {[key: string]: any}
    stage_data_lift(params: {
          p_stage: pkg_report_schedule_guntt;
          p_guntt_calculation_method: guntt_calculation_method;
          p_schedule_list: pkg_report_schedule_guntt[];
        }): pkg_report_schedule_guntt
  }
  pkg_schedule_version: {
    version_access_check(params: {
          p_schedule_version_id: number;
          p_user_id: number;
        }): void
    version_check(params: {
          p_schedule_version_id: number;
          p_object_id?: number;
          p_allow_null?: boolean;
        }): number
    version_checkup(params: {
          p_user_id: number;
          p_object_id: number;
        }): number
    version_create(params: {
          p_user_id: number;
          p_object_id: number;
          p_name: string;
          p_current?: boolean;
          p_copy_from_schedule_version_id?: number;
          p_meta?: {[key: string]: any};
        }): number
    version_delete(params: {
          p_user_id: number;
          p_schedule_version_id: number[];
        }): number[]
    version_get_current(params: {
          p_user_id: number;
          p_object_id: number;
        }): {
            schedule_version_id: number,
            object_id: number,
            name: string,
            current: boolean,
            meta: {[key: string]: any},
          }[]
    version_get_current_by_schedule(params: {
          p_user_id: number;
          p_schedule_id: number;
        }): {
            schedule_version_id: number,
            object_id: number,
            name: string,
            current: boolean,
            meta: {[key: string]: any},
          }[]
    version_list(params: {
          p_user_id: number;
          p_schedule_version_id: number[];
          p_object_id: number[];
          p_deleted?: boolean;
        }): {
            schedule_version_id: number,
            object_id: number,
            current: boolean,
            name: string,
            meta: {[key: string]: any},
            deleted: number,
          }[]
    version_set_current(params: {
          p_user_id: number;
          p_schedule_version_id: number;
        }): number
    version_update(params: {
          p_user_id: number;
          p_schedule_version_id: number;
          p_name: string;
          p_meta: {[key: string]: any};
        }): number
  }
  pkg_suek: {
    budget_plan_check(params: {
          p_budget_plan_id: number;
          p_allow_null?: boolean;
        }): number
    budget_plan_create(params: {
          p_user_id: number;
          p_object_id: number;
          p_date: Date;
          p_expenditure: number;
          p_expenditure_sd: number;
        }): number
    budget_plan_delete(params: {
          p_user_id: number;
          p_budget_plan_id: number[];
        }): number[]
    budget_plan_list(params: {
          p_object_id: number;
          p_budget_plan_id: number[];
        }): {
            object_id: number,
            budget_plan_id: number,
            date: Date,
            expenditure: number,
            expenditure_sd: number,
          }[]
    budget_plan_update(params: {
          p_user_id: number;
          p_budget_plan_id: number;
          p_date: Date;
          p_expenditure: number;
          p_expenditure_sd: number;
        }): number
    rpo_list(params: {
          p_rpo_id: number[];
          p_deleted?: boolean;
        }): {
            rpo_id: number,
            name: string,
            deleted: number,
          }[]
  }
  pkg_user: {
    user_auth(params: {
          p_login: string;
          p_password: string;
          p_address: string;
          p_client: string;
          p_salt?: string;
          p_salt_sha256?: string;
        }): number
    user_check(params: {
          p_user_id: number;
          p_allow_null?: boolean;
        }): number
    user_create(params: {
          p_user_id: number;
          p_organisation_id: number;
          p_firstname_ru: string;
          p_lastname_ru: string;
          p_patronymic_ru: string;
          p_firstname_en: string;
          p_lastname_en: string;
          p_patronymic_en: string;
          p_birthdate: Date;
          p_email: string;
          p_phone: string;
          p_password: string;
          p_salt: string;
          p_meta?: {[key: string]: any};
          p_user_object_id?: string;
          p_external_data?: {[key: string]: any};
        }): number
    user_data_get(params: {
          p_user_id: number;
        }): {[key: string]: any}
    user_data_set(params: {
          p_user_id: number;
          p_data: {[key: string]: any};
        }): number
    user_delete(params: {
          p_action_user_id: number;
          p_user_id: number[];
        }): number[]
    user_favorite_add(params: {
          p_user_id: number;
          p_relation: relation;
          p_relation_id: number[];
        }): number
    user_favorite_delete(params: {
          p_user_id: number;
          p_relation: relation;
          p_relation_id: number[];
        }): number
    user_list(params: {
          p_action_user_id: number;
          p_user_id?: number[];
          p_organisation_id?: number[];
          p_deleted?: boolean;
          p_filter?: {[key: string]: any};
        }): {
            user_id: number,
            organisation_id: number,
            firstname_ru: string,
            lastname_ru: string,
            patronymic_ru: string,
            firstname_en: string,
            lastname_en: string,
            patronymic_en: string,
            birthdate: Date,
            email: string,
            phone: string,
            deleted: number,
          }[]
    user_list_by_any_role_in_relation(params: {
          p_action_user_id: number;
          p_relation: relation;
          p_relation_id: number[];
        }): {
            user_id: number,
            organisation_id: number,
            firstname_ru: string,
            lastname_ru: string,
            patronymic_ru: string,
            firstname_en: string,
            lastname_en: string,
            patronymic_en: string,
            birthdate: Date,
            email: string,
            phone: string,
            deleted: number,
          }[]
    user_organisation_check(params: {
          p_action_user_id: number;
          p_user_id: number[];
        }): void
    user_permission_clone(params: {
          p_source_user_id: number;
          p_target_user_id: number;
        }): void
    user_relation_permission_check(params: {
          p_user_id: number;
          p_relation: relation;
          p_relation_id: number;
          p_permission_alias: string;
        }): boolean
    user_relation_permission_list(params: {
          p_user_id: number;
          p_relation: relation;
          p_relation_id?: number[];
        }): {
            relation_id: number,
            permission: string[],
          }[]
    user_relation_role_list(params: {
          p_user_id: number;
          p_relation: relation;
          p_relation_id?: number[];
        }): {
            relation_id: number,
            relation: relation,
            role: string[],
          }[]
    user_relation_role_set(params: {
          p_action_user_id: number;
          p_relation: relation;
          p_relation_id: number[];
          p_user_id: number[];
          p_role_id: number[];
          p_append: boolean;
        }): void
    user_search_by_email(params: {
          p_user_id: number;
          p_email: string;
        }): {
            user_id: number,
            organisation_id: number,
            firstname_ru: string,
            lastname_ru: string,
            patronymic_ru: string,
            firstname_en: string,
            lastname_en: string,
            patronymic_en: string,
            birthdate: Date,
            email: string,
            phone: string,
            deleted: number,
          }[]
    user_session_permission_check(params: {
          p_user_id: number;
          p_session_id: number;
        }): void
    user_update(params: {
          p_action_user_id: number;
          p_user_id: number;
          p_firstname_ru: string;
          p_lastname_ru: string;
          p_patronymic_ru: string;
          p_firstname_en: string;
          p_lastname_en: string;
          p_patronymic_en: string;
          p_birthdate: Date;
          p_email: string;
          p_phone: string;
          p_password: string;
          p_salt: string;
          p_meta?: {[key: string]: any};
        }): number
  }
  pkg_util: {
    bool_gui(params: {
          p_bool: boolean;
        }): number
    generate_random_string(params: {
          p_length: number;
          p_type?: string;
        }): string
    timestamp_utc(): Date
    unix_timestamp_utc(): number
  }
};

export const DbSchemaParams = {
  pkg_admin: {
    admin_auth: [
          'p_email',
          'p_password',
          'p_address',
          'p_client',
          'p_salt?',
        ],
    admin_check: [
          'p_admin_id',
        ],
    admin_data_get: [
          'p_admin_id',
        ],
    admin_data_set: [
          'p_admin_id',
          'p_data',
        ],
    admin_list: [
          'p_admin_id',
        ],
    admin_session_check: [
          'p_session_id',
        ],
    admin_session_create: [
          'p_admin_id',
          'p_address',
          'p_client',
        ],
    admin_session_delete: [
          'p_admin_id',
          'p_session_id',
        ],
    admin_session_list: [
          'p_action_admin_id',
          'p_admin_id',
        ],
  },
  pkg_analytics: {
    analytics_template_check: [
          'p_analytics_template_id',
          'p_allow_null?',
        ],
    analytics_template_create: [
          'p_user_id',
          'p_alias',
          'p_permission',
          'p_template_type',
          'p_name',
          'p_comment',
          'p_template',
          'p_settings',
          'p_sort_order?',
        ],
    analytics_template_delete: [
          'p_user_id',
          'p_analytics_template_id',
        ],
    analytics_template_list: [
          'p_user_id',
          'p_analytics_template_id',
          'p_filter?',
        ],
    analytics_template_update: [
          'p_user_id',
          'p_analytics_template_id',
          'p_alias',
          'p_permission',
          'p_template_type',
          'p_name',
          'p_comment',
          'p_template',
          'p_settings',
          'p_sort_order?',
        ],
    analytics_template_user_template_list: [
          'p_user_id',
          'p_filter?',
        ],
    suek_daily_fact_entering_report: [
          'p_user_id',
          'p_date?',
        ],
    suek_daily_fact_entering_report_json: [
          'p_user_id',
          'p_date?',
        ],
    suek_fact_entering_report: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_fact_entering_report_json: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_fact_entering_rpo_and_daily_report_json: [
          'p_user_id',
          'p_date?',
        ],
    suek_object_budget_daily_report: [
          'p_user_id',
          'p_date?',
        ],
    suek_object_budget_daily_report_json: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_object_detail_report: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_object_detail_report_json: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_object_fact_status_report: [
          'p_user_id',
          'p_date?',
        ],
    suek_object_fact_status_report_json: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_object_readiness_report: [
          'p_user_id',
          'p_date_to',
        ],
    suek_object_readiness_report_json: [
          'p_user_id',
          'p_date_to?',
        ],
    suek_rpo_fact_entering_report: [
          'p_user_id',
          'p_date?',
        ],
    suek_rpo_fact_entering_report_json: [
          'p_user_id',
          'p_date?',
        ],
    suek_rpo_object_readiness_fact_image_list: [
          'p_object_id',
        ],
    suek_rpo_object_readiness_fact_image_list_limit: [
          'p_object_id',
          'p_limit?',
        ],
    suek_rpo_object_readiness_report: [
          'p_user_id',
          'p_rpo_id',
          'p_date_current',
          'p_date_previous',
        ],
    suek_rpo_object_readiness_report_json: [
          'p_user_id',
          'p_rpo_id',
          'p_date_current',
          'p_date_previous',
        ],
  },
  pkg_auth: {
    generate_password_hash: [
          'p_password',
          'p_salt',
        ],
    generate_password_sha256_hash: [
          'p_password',
          'p_salt',
        ],
    session_check: [
          'p_session_id',
        ],
    session_create: [
          'p_user_id',
          'p_address',
          'p_client',
        ],
    session_delete: [
          'p_user_id',
          'p_session_id',
        ],
    session_list: [
          'p_action_user_id',
          'p_user_id',
        ],
  },
  pkg_db_util: {
    column_exists: [
          'p_column',
          'p_table',
          'p_schema?',
        ],
    grant_all_priveleges: [
          'database',
          'grant_to',
        ],
    grant_read_priveleges: [
          'database',
          'grant_to',
        ],
    pg_column_list: [
          'p_schema?',
          'p_table?',
        ],
    pg_constraint_exists: [
          'p_schema',
          'p_table',
          'p_constraint',
        ],
    pg_constraint_list: [
          'p_schema?',
          'p_constraint_type?',
        ],
    pg_type_exists: [
          'p_schema',
          'p_typename',
        ],
    restart_all_seq: [],
  },
  pkg_exception: {
    message_list: [],
    message_list_routine_create: [],
    parse_routine_definition: [
          'p_body',
        ],
    routine_definition: [
          'p_schema',
          'p_routine',
          'p_arguments',
        ],
    routine_exception_list: [],
    routine_list: [
          'p_schema_pattern',
        ],
  },
  pkg_experimental: {
    object_schedule_level_set: [
          'p_object_id',
        ],
    schedule_monthly_expenditure_list: [
          'p_object_id',
        ],
  },
  pkg_file: {
    file_check: [
          'p_file_id',
          'p_allow_null?',
        ],
    file_create: [
          'p_user_id',
          'p_file_relation',
          'p_file_relation_id',
          'p_parent_id?',
          'p_public?',
        ],
    file_delete: [
          'p_user_id',
          'p_file_id',
        ],
    file_list: [
          'p_user_id',
          'p_file_id',
          'p_deleted?',
        ],
    file_move: [
          'p_user_id',
          'p_file_id',
          'p_parent_id',
          'p_name?',
        ],
    file_path: [
          'p_file_id',
        ],
    file_relation_list: [
          'p_user_id',
          'p_file_relation',
          'p_file_relation_id',
          'p_parent_id',
          'p_filter?',
          'p_sort?',
          'p_deleted?',
        ],
    file_update: [
          'p_user_id',
          'p_file_id',
          'p_mimetype',
          'p_name',
          'p_size',
          'p_ext',
          'p_digest',
          'p_uploaded',
          'p_meta?',
        ],
    file_user_list: [
          'p_user_id',
          'p_filter?',
          'p_sort?',
          'p_deleted?',
        ],
    folder_create: [
          'p_user_id',
          'p_file_relation',
          'p_file_relation_id',
          'p_name',
          'p_parent_id?',
        ],
    folder_delete: [
          'p_user_id',
          'p_file_id',
        ],
    folder_update: [
          'p_user_id',
          'p_file_id',
          'p_name',
        ],
  },
  pkg_history: {
    history_accessible_relations_list: [
          'p_user_id',
        ],
    history_action_enum_list: [],
    history_create: [
          'p_user_id',
          'p_action',
          'p_history_relation',
          'p_history_relation_id',
          'p_old_record',
          'p_new_record',
        ],
    history_filter_data: [
          'p_user_id',
          'p_history_relation',
        ],
    history_list: [
          'p_user_id',
          'p_filter',
          'p_sort',
          'p_limit',
          'p_offset',
        ],
    history_relation_enum_list: [],
    history_relation_list: [
          'p_history_relation',
          'p_history_relation_id',
        ],
  },
  pkg_image: {
    image_check: [
          'p_image_id',
          'p_allow_null?',
        ],
    image_convert_relation_list: [
          'p_user_id',
          'p_image_relation',
        ],
    image_create: [
          'p_user_id',
          'p_image_relation',
          'p_image_relation_id',
        ],
    image_delete: [
          'p_user_id',
          'p_image_id',
        ],
    image_list: [
          'p_user_id',
          'p_image_id',
        ],
    image_relation_list: [
          'p_user_id',
          'p_image_relation',
          'p_image_relation_id',
          'p_sort',
          'p_main?',
        ],
    image_set_main: [
          'p_user_id',
          'p_image_id',
        ],
    image_update: [
          'p_user_id',
          'p_image_id',
          'p_mimetype',
          'p_name',
          'p_size',
          'p_ext',
          'p_digest',
          'p_uploaded',
          'p_converted?',
          'p_meta?',
          'p_geo_coord?',
          'p_exif?',
        ],
  },
  pkg_integration: {
    permission_list: [
          'p_user_id',
          'p_relation',
          'p_relation_id?',
        ],
    user_list: [
          'p_page',
          'p_per_page',
          'p_user_id?',
          'p_search?',
          'p_relation?',
          'p_relation_id?',
        ],
  },
  pkg_log: {
    exception_log_add: [
          'p_code',
          'p_message',
          'p_query',
          'p_fname?',
          'p_label?',
        ],
    exception_log_list: [],
    exception_log_partition_create: [
          'date_from',
          'count',
        ],
  },
  pkg_object: {
    object_access_check: [
          'p_user_id',
          'p_object_id',
        ],
    object_check: [
          'p_object_id',
          'p_allow_null?',
        ],
    object_create: [
          'p_user_id',
          'p_project_id',
          'p_object_type_id',
          'p_region_id',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_address',
          'p_geo_coord',
          'p_broadcast_url',
          'p_passport_data_ru',
          'p_passport_data_en',
          'p_calculation_method',
          'p_meta',
        ],
    object_delete: [
          'p_user_id',
          'p_object_id',
        ],
    object_get_as_related_json: [
          'p_object_id',
        ],
    object_get_by_fact: [
          'p_user_id',
          'p_schedule_fact_id',
        ],
    object_get_by_schedule: [
          'p_user_id',
          'p_schedule_id',
        ],
    object_grant_access_to_user: [
          'p_user_id',
          'p_object_id',
          'p_role_id',
        ],
    object_group_grant_access_to_user: [
          'p_user_id',
          'p_object_id',
          'p_role_id',
        ],
    object_list: [
          'p_user_id',
          'p_object_id',
          'p_project_id',
          'p_filter?',
          'p_sort?',
          'p_deleted?',
        ],
    object_reset_weighting_factor: [
          'p_user_id',
          'p_object_id',
        ],
    object_settings_set: [
          'p_user_id',
          'p_object_id',
          'p_guntt_status_gray?',
        ],
    object_transfer: [
          'p_user_id',
          'p_object_id',
          'p_project_id',
        ],
    object_update: [
          'p_user_id',
          'p_object_id',
          'p_object_type_id',
          'p_region_id',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_address',
          'p_geo_coord',
          'p_broadcast_url',
          'p_passport_data_ru',
          'p_passport_data_en',
          'p_calculation_method',
          'p_meta',
        ],
    object_user_list: [
          'p_user_id',
          'p_project_id',
          'p_object_id?',
          'p_filter?',
          'p_sort?',
          'p_deleted?',
        ],
    object_user_passport_filter_data: [
          'p_user_id',
        ],
  },
  pkg_organisation: {
    organisation_check: [
          'p_organisation_id',
          'p_allow_null?',
        ],
    organisation_create: [
          'p_user_id',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_meta',
          'p_settings?',
          'p_org_object_id?',
          'p_external_data?',
        ],
    organisation_delete: [
          'p_user_id',
          'p_organisation_id',
        ],
    organisation_list: [
          'p_user_id',
          'p_organisation_id',
          'p_deleted?',
        ],
    organisation_update: [
          'p_user_id',
          'p_organisation_id',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_meta?',
          'p_settings?',
        ],
  },
  pkg_permission: {
    permission_alias_check: [
          'p_permission_alias',
          'p_allow_null?',
        ],
    permission_check: [
          'p_permission_id',
          'p_allow_null?',
        ],
    permission_create: [
          'p_user_id',
          'p_alias',
          'p_name',
        ],
    permission_delete: [
          'p_user_id',
          'p_permission_id',
        ],
    permission_list: [
          'p_permission_id',
        ],
    permission_update: [
          'p_user_id',
          'p_permission_id',
          'p_alias',
          'p_name',
        ],
    role_access_check: [
          'p_user_id',
          'p_role_id',
        ],
    role_alias_check: [
          'p_role_alias',
          'p_allow_null?',
        ],
    role_check: [
          'p_role_id',
          'p_allow_null?',
        ],
    role_create: [
          'p_user_id',
          'p_alias',
          'p_name',
          'p_description?',
          'p_organisation_id?',
        ],
    role_create_default_role_with_permission: [],
    role_create_with_permission: [
          'p_role_alias',
          'p_role_name',
          'p_permission_list',
        ],
    role_delete: [
          'p_user_id',
          'p_role_id',
        ],
    role_list: [
          'p_user_id',
          'p_role_id',
          'p_organisation_id?',
        ],
    role_permission_list: [
          'p_user_id',
          'p_role_id',
          'p_organisation_id?',
          'p_deleted?',
        ],
    role_permission_set: [
          'p_user_id',
          'p_role_id',
          'p_permission_id',
          'p_append?',
        ],
    role_set_to_default_accounts: [],
    role_total_permission_reset: [],
    role_update: [
          'p_user_id',
          'p_role_id',
          'p_alias',
          'p_name',
          'p_description',
          'p_organisation_id',
        ],
  },
  pkg_project: {
    project_access_check: [
          'p_user_id',
          'p_project_id',
        ],
    project_check: [
          'p_project_id',
          'p_allow_null?',
        ],
    project_create: [
          'p_user_id',
          'p_organisation_id',
          'p_name_ru',
          'p_name_en',
          'p_region_id',
          'p_project_type_id',
          'p_description_ru',
          'p_description_en',
          'p_passport_data_ru',
          'p_passport_data_en',
          'p_meta',
          'p_project_guid?',
          'p_external_data?',
        ],
    project_delete: [
          'p_user_id',
          'p_project_id',
        ],
    project_get_as_related_json: [
          'p_project_id',
        ],
    project_grant_access_to_user: [
          'p_user_id',
          'p_project_id',
          'p_role_id',
        ],
    project_group_grant_access_to_user: [
          'p_user_id',
          'p_project_id',
          'p_role_id',
          'p_grant_object',
        ],
    project_list: [
          'p_user_id',
          'p_project_id',
          'p_organisation_id',
          'p_filter?',
          'p_sort?',
          'p_deleted?',
        ],
    project_permission_list: [
          'p_user_id',
          'p_relation',
          'p_relation_id?',
        ],
    project_settings_set: [
          'p_user_id',
          'p_project_id',
          'p_round_value?',
          'p_round_percent?',
          'p_round_expenditure?',
          'p_round_day?',
          'p_schedule_update_requires_comment?',
        ],
    project_transfer: [
          'p_user_id',
          'p_project_id',
          'p_organisation_id',
        ],
    project_update: [
          'p_user_id',
          'p_project_id',
          'p_name_ru',
          'p_name_en',
          'p_region_id',
          'p_project_type_id',
          'p_description_ru',
          'p_description_en',
          'p_passport_data_ru',
          'p_passport_data_en',
          'p_meta',
        ],
    project_user_list: [
          'p_user_id',
          'p_project_id?',
          'p_filter?',
          'p_sort?',
          'p_deleted?',
        ],
    project_user_passport_filter_data: [
          'p_user_id',
        ],
  },
  pkg_reference: {
    calculation_method_list: [],
    element_check: [
          'p_element_id',
          'p_allow_null?',
        ],
    element_create: [
          'p_user_id',
          'p_code',
          'p_key',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_job_element_group',
        ],
    element_delete: [
          'p_user_id',
          'p_element_id',
        ],
    element_list: [
          'p_user_id',
          'p_element_id',
          'p_filter?',
          'p_deleted?',
        ],
    element_update: [
          'p_user_id',
          'p_element_id',
          'p_code',
          'p_key',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_job_element_group',
        ],
    fact_correction_status_list: [],
    fact_type_list: [],
    image_relation_type_list: [],
    job_check: [
          'p_job_id',
          'p_allow_null?',
        ],
    job_create: [
          'p_user_id',
          'p_code',
          'p_key',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_job_element_group',
          'p_unit',
        ],
    job_delete: [
          'p_user_id',
          'p_job_id',
        ],
    job_element_group_list: [],
    job_list: [
          'p_user_id',
          'p_job_id',
          'p_filter?',
          'p_deleted?',
        ],
    job_unit_list: [],
    job_update: [
          'p_user_id',
          'p_job_id',
          'p_code',
          'p_key',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_job_element_group',
          'p_unit',
        ],
    object_type_check: [
          'p_object_type_id',
          'p_allow_null?',
        ],
    object_type_create: [
          'p_user_id',
          'p_alias',
          'p_name_ru',
          'p_name_en',
          'p_description_ru?',
          'p_description_en?',
          'p_passport_template_ru?',
          'p_passport_template_en?',
        ],
    object_type_delete: [
          'p_user_id',
          'p_object_type_id',
        ],
    object_type_list: [
          'p_object_type_id',
          'p_deleted?',
        ],
    object_type_update: [
          'p_user_id',
          'p_object_type_id',
          'p_alias',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_passport_template_ru',
          'p_passport_template_en',
        ],
    project_type_check: [
          'p_project_type_id',
          'p_allow_null?',
        ],
    project_type_create: [
          'p_user_id',
          'p_alias',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_passport_template_ru?',
          'p_passport_template_en?',
        ],
    project_type_delete: [
          'p_user_id',
          'p_project_type_id',
        ],
    project_type_list: [
          'p_project_type_id',
          'p_deleted?',
        ],
    project_type_update: [
          'p_user_id',
          'p_project_type_id',
          'p_alias',
          'p_name_ru',
          'p_name_en',
          'p_description_ru',
          'p_description_en',
          'p_passport_template_ru',
          'p_passport_template_en',
        ],
    region_list: [
          'p_region_id',
          'p_deleted?',
        ],
  },
  pkg_report: {
    report_fact_foto: [
          'p_user_id',
          'p_object_id',
          'p_date_from?',
          'p_date_to?',
          'p_fact_type?',
        ],
    report_fact_frequency: [
          'p_user_id',
          'p_object_id',
          'p_date_from',
          'p_date_to',
          'p_fact_type',
          'p_allow_expenditure?',
          'p_fact_source?',
        ],
    report_object_arrow: [
          'p_user_id',
          'p_object_id',
          'p_schedule_version_id?',
          'p_fact_type?',
          'p_calculation_method?',
        ],
    report_object_arrow_list: [
          'p_user_id',
          'p_report_arrow_id',
          'p_schedule_version_id?',
          'p_fact_type?',
          'p_calculation_method?',
        ],
    report_object_guntt: [
          'p_user_id',
          'p_object_id',
          'p_schedule_version_id?',
          'p_fact_type?',
          'p_calculation_method?',
          'p_allow_expenditure?',
          'p_date_to?',
        ],
    report_object_guntt_fact_sum: [
          'p_schedule_id',
          'p_fact_type',
          'p_date_to',
        ],
    report_object_guntt_list: [
          'p_object_id',
          'p_schedule_version_id?',
          'p_allow_expenditure?',
        ],
  },
  pkg_schedule: {
    schedule_check: [
          'p_schedule_id',
          'p_allow_null?',
        ],
    schedule_create: [
          'p_user_id',
          'p_object_id',
          'p_schedule_version_id',
          'p_parent_id',
          'p_name',
          'p_job_id',
          'p_job_unit',
          'p_element_id',
          'p_date_start',
          'p_date_finish',
          'p_volume_actual',
          'p_expenditure_actual',
          'p_volume_planned',
          'p_expenditure_planned',
          'p_weighting_factor?',
          'p_reset_weighting_factor?',
          'p_meta?',
          'p_comment?',
        ],
    schedule_delete: [
          'p_user_id',
          'p_schedule_id',
          'p_reset_weighting_factor?',
          'p_comment?',
        ],
    schedule_get_as_related_json: [
          'p_schedule_id',
          'p_schedule_version_id?',
        ],
    schedule_list: [
          'p_user_id',
          'p_object_id',
          'p_schedule_id',
          'p_schedule_version_id',
          'p_allow_expenditure?',
          'p_allow_weighting_factor?',
          'p_deleted?',
          'p_filter?',
        ],
    schedule_load: [
          'p_user_id',
          'p_object_id',
          'p_filename',
          'p_data',
          'p_expenditure_multiplier?',
        ],
    schedule_move: [
          'p_user_id',
          'p_schedule_id',
          'p_parent_id',
          'p_after_schedule_id',
        ],
    schedule_organisation_user_check: [
          'p_schedule_id',
          'p_user_id',
        ],
    schedule_path_get: [
          'p_object_id',
          'p_parent_id',
        ],
    schedule_path_piece_length: [],
    schedule_path_reset_by_sort_order: [
          'p_object_id',
          'p_parent_id',
          'p_path',
        ],
    schedule_sort_step: [],
    schedule_update: [
          'p_user_id',
          'p_schedule_id',
          'p_schedule_version_id',
          'p_name',
          'p_job_id',
          'p_job_unit',
          'p_element_id',
          'p_date_start',
          'p_date_finish',
          'p_volume_actual',
          'p_expenditure_actual',
          'p_volume_planned',
          'p_expenditure_planned',
          'p_weighting_factor?',
          'p_reset_weighting_factor?',
          'p_meta?',
          'p_comment?',
        ],
  },
  pkg_schedule_fact: {
    fact_access_check: [
          'p_user_id',
          'p_schedule_fact_id',
        ],
    fact_check: [
          'p_schedule_fact_id',
          'p_allow_null?',
        ],
    fact_create: [
          'p_user_id',
          'p_schedule_id',
          'p_fact_type',
          'p_fact_source',
          'p_volume',
          'p_expenditure',
          'p_employee',
          'p_comment?',
          'p_created?',
        ],
    fact_delete: [
          'p_user_id',
          'p_schedule_fact_id',
        ],
    fact_get_as_related_json: [
          'p_schedule_fact_id',
        ],
    fact_list: [
          'p_user_id',
          'p_schedule_fact_id',
          'p_filter?',
          'p_sort?',
          'p_allow_expenditure?',
          'p_allow_employee?',
          'p_deleted?',
        ],
    fact_update: [
          'p_user_id',
          'p_schedule_fact_id',
          'p_volume',
          'p_expenditure',
          'p_employee',
          'p_comment?',
        ],
  },
  pkg_schedule_fact_correction: {
    fact_correction_check: [
          'p_schedule_fact_correction_id',
          'p_allow_null?',
        ],
    fact_correction_create: [
          'p_user_id',
          'p_object_id',
          'p_status',
          'p_date_from',
          'p_date_to',
        ],
    fact_correction_data_check: [
          'p_schedule_fact_correction_data_id',
          'p_allow_null?',
        ],
    fact_correction_data_get: [
          'p_user_id',
          'p_schedule_fact_correction_id',
        ],
    fact_correction_data_load: [
          'p_user_id',
          'p_object_id',
          'p_date_from',
          'p_date_to',
        ],
    fact_correction_data_set: [
          'p_user_id',
          'p_schedule_fact_correction_id',
          'p_data',
        ],
    fact_correction_list: [
          'p_user_id',
          'p_object_id',
          'p_schedule_fact_correction_id',
          'p_filter',
          'p_sort',
          'p_limit',
          'p_offset',
        ],
    fact_correction_update: [
          'p_user_id',
          'p_schedule_fact_correction_id',
          'p_status',
          'p_date_from',
          'p_date_to',
        ],
  },
  pkg_schedule_plan: {
    plan_check: [
          'p_schedule_plan_id',
          'p_allow_null?',
        ],
    plan_checkup: [
          'p_user_id',
          'p_schedule_id',
          'p_schedule_version_id',
          'p_date_start',
          'p_date_finish',
          'p_volume_planned',
          'p_expenditure_planned',
        ],
    plan_create: [
          'p_user_id',
          'p_schedule_id',
          'p_schedule_version_id',
          'p_date_start',
          'p_date_finish',
          'p_volume_planned',
          'p_expenditure_planned',
        ],
    plan_delete: [
          'p_user_id',
          'p_schedule_plan_id',
        ],
    plan_list: [
          'p_user_id',
          'p_schedule_plan_id',
          'p_schedule_id',
        ],
  },
  pkg_schedule_util: {
    job_deviation_day: [
          'p_date_start',
          'p_date_finish',
          'p_volume_fact_and_actual',
          'p_volume_planned',
        ],
    job_status: [
          'p_job_id',
          'p_date_start',
          'p_date_finish',
          'p_volume_fact',
          'p_volume_planned',
          'p_date_to?',
        ],
    job_volume_percent: [
          'p_volume_planned',
          'p_volume_actual',
          'p_calculation_method',
          'p_weighting_factor',
        ],
    object_calculate_and_update_guntt_fields: [
          'p_object_id?',
          'p_project_id?',
        ],
    project_calculate_and_update_guntt_fields: [
          'p_project_id?',
        ],
    relation_calculate_and_update_guntt_fields: [
          'p_relation',
          'p_relation_id',
        ],
    schedule_fact_frequency: [
          'p_schedule_id',
          'p_date_from',
          'p_date_to',
          'p_fact_type',
          'p_allow_expenditure?',
          'p_fact_source?',
        ],
    stage_data_lift: [
          'p_stage',
          'p_guntt_calculation_method',
          'p_schedule_list',
        ],
  },
  pkg_schedule_version: {
    version_access_check: [
          'p_schedule_version_id',
          'p_user_id',
        ],
    version_check: [
          'p_schedule_version_id',
          'p_object_id?',
          'p_allow_null?',
        ],
    version_checkup: [
          'p_user_id',
          'p_object_id',
        ],
    version_create: [
          'p_user_id',
          'p_object_id',
          'p_name',
          'p_current?',
          'p_copy_from_schedule_version_id?',
          'p_meta?',
        ],
    version_delete: [
          'p_user_id',
          'p_schedule_version_id',
        ],
    version_get_current: [
          'p_user_id',
          'p_object_id',
        ],
    version_get_current_by_schedule: [
          'p_user_id',
          'p_schedule_id',
        ],
    version_list: [
          'p_user_id',
          'p_schedule_version_id',
          'p_object_id',
          'p_deleted?',
        ],
    version_set_current: [
          'p_user_id',
          'p_schedule_version_id',
        ],
    version_update: [
          'p_user_id',
          'p_schedule_version_id',
          'p_name',
          'p_meta',
        ],
  },
  pkg_suek: {
    budget_plan_check: [
          'p_budget_plan_id',
          'p_allow_null?',
        ],
    budget_plan_create: [
          'p_user_id',
          'p_object_id',
          'p_date',
          'p_expenditure',
          'p_expenditure_sd',
        ],
    budget_plan_delete: [
          'p_user_id',
          'p_budget_plan_id',
        ],
    budget_plan_list: [
          'p_object_id',
          'p_budget_plan_id',
        ],
    budget_plan_update: [
          'p_user_id',
          'p_budget_plan_id',
          'p_date',
          'p_expenditure',
          'p_expenditure_sd',
        ],
    rpo_list: [
          'p_rpo_id',
          'p_deleted?',
        ],
  },
  pkg_user: {
    user_auth: [
          'p_login',
          'p_password',
          'p_address',
          'p_client',
          'p_salt?',
          'p_salt_sha256?',
        ],
    user_check: [
          'p_user_id',
          'p_allow_null?',
        ],
    user_create: [
          'p_user_id',
          'p_organisation_id',
          'p_firstname_ru',
          'p_lastname_ru',
          'p_patronymic_ru',
          'p_firstname_en',
          'p_lastname_en',
          'p_patronymic_en',
          'p_birthdate',
          'p_email',
          'p_phone',
          'p_password',
          'p_salt',
          'p_meta?',
          'p_user_object_id?',
          'p_external_data?',
        ],
    user_data_get: [
          'p_user_id',
        ],
    user_data_set: [
          'p_user_id',
          'p_data',
        ],
    user_delete: [
          'p_action_user_id',
          'p_user_id',
        ],
    user_favorite_add: [
          'p_user_id',
          'p_relation',
          'p_relation_id',
        ],
    user_favorite_delete: [
          'p_user_id',
          'p_relation',
          'p_relation_id',
        ],
    user_list: [
          'p_action_user_id',
          'p_user_id?',
          'p_organisation_id?',
          'p_deleted?',
          'p_filter?',
        ],
    user_list_by_any_role_in_relation: [
          'p_action_user_id',
          'p_relation',
          'p_relation_id',
        ],
    user_organisation_check: [
          'p_action_user_id',
          'p_user_id',
        ],
    user_permission_clone: [
          'p_source_user_id',
          'p_target_user_id',
        ],
    user_relation_permission_check: [
          'p_user_id',
          'p_relation',
          'p_relation_id',
          'p_permission_alias',
        ],
    user_relation_permission_list: [
          'p_user_id',
          'p_relation',
          'p_relation_id?',
        ],
    user_relation_role_list: [
          'p_user_id',
          'p_relation',
          'p_relation_id?',
        ],
    user_relation_role_set: [
          'p_action_user_id',
          'p_relation',
          'p_relation_id',
          'p_user_id',
          'p_role_id',
          'p_append',
        ],
    user_search_by_email: [
          'p_user_id',
          'p_email',
        ],
    user_session_permission_check: [
          'p_user_id',
          'p_session_id',
        ],
    user_update: [
          'p_action_user_id',
          'p_user_id',
          'p_firstname_ru',
          'p_lastname_ru',
          'p_patronymic_ru',
          'p_firstname_en',
          'p_lastname_en',
          'p_patronymic_en',
          'p_birthdate',
          'p_email',
          'p_phone',
          'p_password',
          'p_salt',
          'p_meta?',
        ],
  },
  pkg_util: {
    bool_gui: [
          'p_bool',
        ],
    generate_random_string: [
          'p_length',
          'p_type?',
        ],
    timestamp_utc: [],
    unix_timestamp_utc: [],
  },
};
