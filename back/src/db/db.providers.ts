import { createConnection } from 'typeorm';
require ('dotenv').config();

const provide = 'DB_CONNECT';
const type = 'postgres';
const host = process.env.APP_DB_HOST;
const port = Number(process.env.APP_DB_PORT);
const username = process.env.APP_DB_USERNAME;
const password = process.env.APP_DB_PASSWORD;
const database = process.env.APP_DB_NAME;

export const dbProviders = [
  {
    provide,
    useFactory: async () => {
      try {
        return await createConnection({
          type,
          host,
          port,
          username,
          password,
          database,
          entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
          synchronize: false,
        });
      } catch (err) {
        console.log({err})
        throw new Error('Connecting database failed!');
      }
    },
  },
];
