export class ElementCreateRequestDTO {
  parent_id: number;
  job_id: number;
  name: string;
}

export class ElementListRequestDTO {
  element_id: number[];
}

export class ElementResponseDTO {
  element_id: number;
  parent_id: number;
  job_id: number;
  name: string;
}
