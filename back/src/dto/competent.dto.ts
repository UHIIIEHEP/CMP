import { userInfoBySession } from "./user.dto";


export class CompetentRelationListRequestDTO extends userInfoBySession {
  relation: string;
  relation_id: number;
}

export class CompetentUserSelfListRequestDTO extends userInfoBySession {
}

export class CompetentRelationSetRequestDTO extends userInfoBySession {
  competent_id: number[];
  relation: string;
  relation_id: number;
  append: boolean;
}

export class CompetentListRequestDTO extends userInfoBySession {
  competent_id: number[];
}

export class CompetentResponseDTO {
  competent_id: number;
  name: string;
}
