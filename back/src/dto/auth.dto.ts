class Auth {
  login: string;
  password: string;
}

export class AuthLoginRequestDTO extends Auth {}

export class AuthResponseDTO {
  token: string;
}
