import { passwordIndexMap, procedureRegexp, envBlacklist } from './constants';

/** Function checks if index of p_password argument of procedure is the same */
const isPassword = (procedure: string, index: Number): boolean => {
  return passwordIndexMap[procedure] === index;
};

/** Function replaces some sensitive params from postgres error message */
const paramHandler = (
  param: string,
  index: Number,
  log: { [key: string]: any },
  ): string => {

  // replace blacklisted env values with key name
  if (Object.values(envBlacklist).includes(param)) {
    return Object.keys(envBlacklist).find(key => envBlacklist[key] === param);
  }

  // replace actual password with 'password'
  if (typeof log.query === 'string') {
    const match = log.query.match(procedureRegexp);
    if (match) {
      if (isPassword(match[0], index)) {
        return 'password';
      }
    }
  }

  return param;
};

export const filterError = (log: any): any => {
  if (typeof log === 'object' && log !== null && Array.isArray(log?.parameters))
    return {
      ...log,
      parameters: log.parameters
        .map((param, index) => paramHandler(param, index, log)),
    };
  return log;
};
