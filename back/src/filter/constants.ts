// index of p_password param in various db procedures
export const passwordIndexMap = {
  'pkg_user.user_create': 11,
  'pkg_user.user_update': 11,
  'pkg_user.user_auth': 1,
  'pkg_auth.generate_password_hash': 0,
  'pkg_auth.generate_password_sha256_hash': 0,
  'pkg_admin.admin_auth': 1,
};

export const envBlacklist = {
  app_user_password_salt: process.env.APP_USER_PASSWORD_SALT,
  app_user_password_salt_old: process.env.APP_USER_PASSWORD_SALT_OLD,
  app_admin_password_salt: process.env.APP_ADMIN_PASSWORD_SALT,
};

export const procedureRegexp = 'pkg_.+\\.[^\\(]+';
