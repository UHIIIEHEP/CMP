import {
  Injectable,
  CanActivate,
  Inject,
  ExecutionContext,
  ForbiddenException,
  UnauthorizedException,
} from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Repository } from "typeorm";
import { UserGuardDTO } from "../../dto/user.dto";
import { dbQuery } from "../../helper";

const jwt = require('jsonwebtoken');


@Injectable()
export class UserGuard implements CanActivate {
  constructor(
    @Inject('USER_REPOSITORY')
    private userCheckRepository: Repository<UserGuardDTO>,
    private readonly reflector: Reflector,
  ) {}
  async canActivate(context: ExecutionContext): Promise<any> {  

    const req = context.switchToHttp().getRequest();

    if (req.headers?.authorization) {
      const jwtToken = req.headers?.authorization?.split(' ');

      if (jwtToken[0].toLowerCase() !== 'bearer') {
        throw new ForbiddenException({
          message: 'Authorization required (2)',
        });
      }

      let decoded;

      try {
        decoded = jwt.verify(jwtToken[1], process.env.USER_SIGNATURE);
      } catch (err) {
        throw new UnauthorizedException('Bad token');
      }

      const sessionId = decoded.session_id;

      const [userInfoBySession] = await dbQuery(
        'pkg_user',
        'user_info_by_session',
        [
          sessionId.user_auth,
        ]
      );

      const permission = this.reflector.get<string[]>('permission', context.getHandler());

      const [{permission_check}] = await dbQuery(
        'pkg_permission',
        'permission_check',
        [
          userInfoBySession.user_id,
          permission,
        ]
      );

      if (!permission_check) {
        throw new ForbiddenException();
      }

      Object.assign(req.body, {
        userInfoBySession,
      });
    } else {
      throw new UnauthorizedException('Bad token');
    }

    return true;
  }
}
