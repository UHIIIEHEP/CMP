const fs = require('fs');
const path = require('path');

const generalFileName = 'util/up.sql';

console.log('start ')

const firstList = ['pkg_db_util'];

const main = async () => {

  const dirname = __dirname.replace(/\/dist/, '');

  await fs.writeFileSync(path.join(dirname, generalFileName), '');
  console.log(`Create file: ${generalFileName}`);

  // const result = await fs.statSync(path.join(dirname, './util/create_schemas.sql'));

  // creaet package
  const pkg = await fs.readdirSync(path.join(dirname, 'schemas'));
  await pkg.forEach(async (pkgName: string) => {
    if (pkgName !== 'public' || !/\w.sql/.test(pkgName)) {
      await fs.appendFileSync(path.join(dirname, generalFileName), `\n\n
        do $$
          begin

            create schema if not exists ${pkgName};

          end
        $$;
      `);
      if (firstList.indexOf(pkgName) !== -1) {
        const scripts = await fs.readdirSync(path.join(dirname, 'schemas', pkgName));
        await scripts.forEach( async (fileName: string) => {
          if (/\w.sql/.test(fileName)) {
            const pathFile = path.join(dirname, 'schemas', pkgName, fileName);
            const data = await fs.readFileSync(pathFile)
            await fs.appendFileSync(path.join(dirname, generalFileName), `\n\n${data}`);
            console.log(fileName, ' OK')
          }
        })
      }
    }
  })



  // add types
  const types = await fs.readdirSync(path.join(dirname, 'schemas/public/type'));
  await types.forEach( async (fileName: string) => {
    const pathFile = path.join(dirname, 'schemas/public/type', fileName);
    const data = await fs.readFileSync(pathFile)
    await fs.appendFileSync(path.join(dirname, generalFileName), `\n\n${data}`);
    console.log(fileName, ' OK')
  })

  //add table
  const table = await fs.readdirSync(path.join(dirname, 'schemas/public'));
  await table.forEach( async (fileName: string) => {
    if (/\w.sql/.test(fileName)) {
      const pathFile = path.join(dirname, 'schemas/public', fileName);
      const data = await fs.readFileSync(pathFile)
      await fs.appendFileSync(path.join(dirname, generalFileName), `\n\n${data}`);
      console.log(fileName, ' OK')
    }
  })

  // add pkg and scripts
  const folders = await fs.readdirSync(path.join(dirname, 'schemas'));
  await folders.forEach(async (pkgName: string) => {
    if (pkgName !== 'public' || !/\w.sql/.test(pkgName)) {
      const scripts = await fs.readdirSync(path.join(dirname, 'schemas', pkgName));

      await scripts.forEach( async (fileName: string) => {
        if (/\w.sql/.test(fileName)) {
          const pathFile = path.join(dirname, 'schemas', pkgName, fileName);
          const data = await fs.readFileSync(pathFile)
          await fs.appendFileSync(path.join(dirname, generalFileName), `\n\n${data}`);
          console.log(fileName, ' OK')
        } else if (fileName === 'data') {
          const scripts = await fs.readdirSync(path.join(dirname, 'schemas', pkgName, 'data'));

          await scripts.forEach( async (fileName: string) => {
            if (/\w.sql/.test(fileName)) {
              const pathFile = path.join(dirname, 'schemas', pkgName, 'data', fileName);
              const data = await fs.readFileSync(pathFile)
              await fs.appendFileSync(path.join(dirname, generalFileName), `\n\n${data}`);
              console.log('schemas', pkgName, 'data', fileName, ' OK')
            }
          })
        }
      })
    }
  })

  return 0;
}

main();
