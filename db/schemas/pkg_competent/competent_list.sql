
create or replace function pkg_competent.competent_list(
  p_competent_id integer[] default null
)

returns table (
  competent_id integer,
  name character varying
)

as $$

  declare

  begin

  return
    query
      select
        p.competent_id,
        p.name
      from
        public.competent p
      where
--         p.deleted is null and
        case
          when
            p_competent_id is not null and array_length(p_competent_id,1) > 0
          then
            p.competent_id = any(p_competent_id)
          else
            true
        end;
  end;
$$

language plpgsql;
