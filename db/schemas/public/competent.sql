do $$
  begin

    -- Sequence

    create sequence if not exists public.competent_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.competent
    (
      competent_id integer default nextval('public.competent_seq'::regclass) not null
    );

    alter sequence public.competent_seq owned by competent.competent_id;

    alter table public.competent add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.competent add column if not exists modified timestamp without time zone default null;
    alter table public.competent add column if not exists created_by integer default null;
    alter table public.competent add column if not exists modified_by integer default null;
    alter table public.competent add column if not exists deleted integer default null;
    alter table public.competent add column if not exists name character varying;
    alter table public.competent alter column name set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'competent', 'pk_competent')) = false) then

            alter table public.competent add
                constraint pk_competent primary key (competent_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
