do $$
  begin

    -- Sequence

    create sequence if not exists public.permission_department_qualification_set_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.permission_department_qualification_set
    (
      permission_department_qualification_set_id integer default nextval('public.permission_department_qualification_set_seq'::regclass) not null
    );

    alter sequence public.permission_department_qualification_set_seq owned by permission_department_qualification_set.permission_department_qualification_set_id;

    alter table public.permission_department_qualification_set add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.permission_department_qualification_set add column if not exists modified timestamp without time zone default null;
    alter table public.permission_department_qualification_set add column if not exists created_by integer default null;
    alter table public.permission_department_qualification_set add column if not exists modified_by integer default null;
    alter table public.permission_department_qualification_set add column if not exists deleted integer default null;
    alter table public.permission_department_qualification_set add column if not exists organisation_id integer;
    alter table public.permission_department_qualification_set alter column organisation_id set not null;
    alter table public.permission_department_qualification_set add column if not exists permission_id integer;
    alter table public.permission_department_qualification_set alter column permission_id set not null;
    alter table public.permission_department_qualification_set add column if not exists department_id integer;
    alter table public.permission_department_qualification_set alter column department_id set not null;
    alter table public.permission_department_qualification_set add column if not exists qualifaication_id integer;
    alter table public.permission_department_qualification_set alter column qualifaication_id set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'permission_department_qualification_set', 'pk_permission_department_qualification_set')) = false) then

          alter table public.permission_department_qualification_set add
            constraint pk_permission_department_qualification_set primary key (permission_department_qualification_set_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
