do $$
  begin

    -- Sequence

    create sequence if not exists public.task_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.task
    (
      task_id integer default nextval('public.task_seq'::regclass) not null
    );

    alter sequence public.task_seq owned by task.task_id;

    alter table public.task add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.task add column if not exists modified timestamp without time zone default null;
    alter table public.task add column if not exists created_by integer default null;
    alter table public.task add column if not exists modified_by integer default null;
    alter table public.task add column if not exists deleted integer default null;

    alter table public.task add column if not exists title character varying;
    alter table public.task alter column title set not null;

    alter table public.task add column if not exists description text;
    alter table public.task alter column description set not null;

    alter table public.task add column if not exists object_id integer;

    alter table public.task add column if not exists accountable_id integer;
    alter table public.task alter column accountable_id set not null;

    alter table public.task add column if not exists assistants integer[];

    alter table public.task add column if not exists application_id integer;

    alter table public.task add column if not exists work_type public.work_type;

    alter table public.task add column if not exists date_to timestamp without time zone default null;
    alter table public.task add column if not exists date_from timestamp without time zone default null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'task', 'pk_task')) = false) then

          alter table public.task add
            constraint pk_task primary key (task_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers


  end
$$;
