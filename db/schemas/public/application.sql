do $$
  begin

    -- Sequence

    create sequence if not exists public.application_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.application
    (
      application_id integer default nextval('public.application_seq'::regclass) not null
    );

    alter sequence public.application_seq owned by application.application_id;

    alter table public.application add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.application add column if not exists modified timestamp without time zone default null;
    alter table public.application add column if not exists created_by integer default null;
    alter table public.application add column if not exists modified_by integer default null;
    alter table public.application add column if not exists deleted integer default null;
    alter table public.application add column if not exists name character varying;
    alter table public.application alter column name set not null;

    -- Constraints
    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'application', 'pk_application')) = false) then

          alter table public.application add constraint pk_application primary key (application_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers


  end
$$;
