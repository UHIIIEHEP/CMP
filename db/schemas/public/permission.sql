do $$
  begin

    -- Sequence

    create sequence if not exists public.permission_seq
      increment 1
      start 1
      minvalue 1
      no maxvalue;

    -- Structure

    create table if not exists public.permission
    (
      permission_id integer default nextval('public.permission_seq'::regclass) not null
    );

    alter sequence public.permission_seq owned by permission.permission_id;

    alter table public.permission add column if not exists created timestamp without time zone default (now() at time zone 'utc');
    alter table public.permission add column if not exists modified timestamp without time zone default null;
    alter table public.permission add column if not exists created_by integer default null;
    alter table public.permission add column if not exists modified_by integer default null;
    alter table public.permission add column if not exists deleted integer default null;
    alter table public.permission add column if not exists name character varying;
    alter table public.permission alter column name set not null;
    alter table public.permission add column if not exists alias character varying;
    alter table public.permission alter column alias set not null;

    -- Constraints

    do $constrain$
      begin

        if ((select * from pkg_db_util.pg_constraint_exists('public', 'permission', 'pk_permission')) = false) then

          alter table public.permission add
            constraint pk_permission primary key (permission_id);

        end if;

      end
    $constrain$;


    -- Indexes


    -- Triggers

  end
$$;
