

create or replace function pkg_organisation.organisation_create(
  p_user_id integer,
  p_organisation_name character varying
)

returns integer

as $$

  declare

    v_organisation_id      integer;

  begin

    insert into public.organisation (
      created_by,
      name
    ) values (
      p_user_id,
      p_organisation_name
    ) returning organisation_id into v_organisation_id;

    return v_organisation_id;

  end;

$$

language plpgsql;