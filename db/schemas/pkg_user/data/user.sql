do $$
  begin

    insert into
      public.user
        (created_by, organisation_id, firstname, lastname, patronymic, login, email, p_hash)
      values

        (-1, 1, 'Egor', 'M', 'A', 'meamea', 'mea@mail.ru', '111111'),
        (-1, 1, 'Ivan', 'Ivanov', 'I', 'iiiiii', 'iiiiii@mail.ru', '111111')

    on conflict do nothing;

  end
$$;