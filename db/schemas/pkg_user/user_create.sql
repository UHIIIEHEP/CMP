-- drop function if exists pkg_user.user_create(integer, integer, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer);

create or replace function pkg_user.user_create (
  p_action_user_id integer,
  p_organisation_id integer,
  p_login character varying,
  p_email character varying,
  p_p_hash character varying,
  p_firstname character varying (100),
  p_lastname character varying (100),
  p_patronymic character varying (100) default null,
  p_qualification_id integer default null,
  p_department_id integer default null,
  p_photo character varying default null
)

returns integer

as $$

declare

    v_user_id integer;

begin

    insert into public.user (
        created_by,
        organisation_id,
        firstname,
        lastname,
        patronymic,
        login,
        email,
        p_hash,
        qualification_id,
        department_id,
        photo
    ) values (
        p_action_user_id,
        p_organisation_id,
        p_firstname,
        p_lastname,
        p_patronymic,
        p_login,
        p_email,
        p_p_hash,
        p_qualification_id,
        p_department_id,
        p_photo
    )
    returning user_id into v_user_id;

    return v_user_id;

end;

$$

language plpgsql;
